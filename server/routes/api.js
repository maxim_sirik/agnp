import express from 'express';
import { getPosts, getPostImages, getPost, getFeaturedPost } from './controllers/posts';
import { getMaps, getMap } from './controllers/maps';
import { search } from './controllers/search';
import { getRecommendedCategories } from './controllers/categories';
import { submitEmail } from './controllers/email';
import {getAboutArticles, getAboutContent} from "./controllers/about";
const router = express.Router();

router.get('/posts', getPosts);
router.get('/posts/images', getPostImages);
router.get('/posts/featured', getFeaturedPost);
router.get('/posts/:slug', getPost);
router.get('/categories', getRecommendedCategories);
router.get('/maps', getMaps);
router.get('/maps/:slug', getMap);
router.get('/search', search);
router.post('/email', submitEmail);
router.get('/about', getAboutContent);
router.get('/about/articles', getAboutArticles);

exports = module.exports = router;
