import keystone from 'keystone';
import createsend from 'createsend-node';

var auth = { apiKey: process.env.CREATESEND_API_KEY };
var api = new createsend(auth);

const Email = keystone.list('Email');

export const submitEmail = (req, res, next) => {
  const email = new Email.model();
  email.set({ email: req.body.email });
  email.save()
    .then(() => {
      var listId = process.env.CREATESEND_LIST_ID
      var details = {
        EmailAddress: req.body.email,
      };
      
      api.subscribers.addSubscriber(listId, details, (err, res) => {
        if (err) console.log(err);
      });

      res.json({ status: 200, message: 'success' });
    })
    .catch((err) => res.json({ status: 400, error: err }))
}