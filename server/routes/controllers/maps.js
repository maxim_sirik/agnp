import keystone from 'keystone';

const City = keystone.list('City');

export const getMaps = (req, res, next) => {
  City.model.find({ state: 'published' }).then((posts) => {
    res.json(posts);
  });
}

export const getMap = (req, res, next) => {
  City.model.findOne({ state: 'published', slug: req.params.slug })
    .populate('places')
    .lean()
    .exec()
    .then((posts) => {
      res.json(posts);
    });
}
