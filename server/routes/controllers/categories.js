import keystone from 'keystone';

const PostCategory = keystone.list('PostCategory');

export const getRecommendedCategories = (req, res, next) => {
  PostCategory.model.find({ recommended: true }).then((categories) => {
    res.json(categories);
  });
}
