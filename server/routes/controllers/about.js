import keystone from 'keystone';

const About = keystone.list('About'),
    AboutArticle = keystone.list('AboutArticle');

export const getAboutContent = (req, res, next) => {
    About.model.findOne()
        .then((content) => {
            res.json(content);
        });
};

export const getAboutArticles = (req, res, next) => {
    AboutArticle.model.find()
        .then((articles) => {
            res.json(articles);
        });
};
