import keystone from 'keystone';
import map from 'lodash/map';
import compact from 'lodash/compact';


const Post = keystone.list('Post');
const City = keystone.list('City');
const Place = keystone.list('Place');

export const search = async (req, res, next) => {
  try {
    const tags = Post.model.find({ tags: req.query.query.toLowerCase() })
      .populate('categories')
      .lean()
      .exec();

    const posts = Post.model.find({ $text: { $search: `"${req.query.query}"` }, state: 'published' })
      .populate('categories')
      .lean()
      .exec();

    const cities = City.model.find({ $text: { $search: `"${req.query.query}"` }, state: 'published' })
      .lean()
      .exec();

    const places = Place.model.find({ $text: { $search: `"${req.query.query}"` }, state: 'published' })
      .lean()
      .exec()
      .then((places) => Promise.all(places.map((place) => {
        return City.model.findOne({ places: place._id })
          .exec()
          .then((city) => {
            if (city && city.state === 'published') {
              place.city = city;
              return place;
            } else {
              return false;
            }
          });
      })));

    Promise.all([tags, cities, places, posts]).then(([tags, cities, places, posts]) => {
      return res.json({
        tagMatches: tags,
        otherMatches: [...cities, ...compact(places), ...posts],
      });
    });

  } catch (err) {
    return res.json({ error: true });
  }
}