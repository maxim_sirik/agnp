import keystone from 'keystone';
import { log } from 'util';

const Post = keystone.list('Post');
const PostCategory = keystone.list('PostCategory');

export const getPosts = (req, res, next) => {
  const filter = req.query.filter || 'all';

  PostCategory.model.findOne({ key: filter }).then((category) => {
    var query = filter === 'all'
      ? { state: 'published', featured: false }
      : { state: 'published', featured: false, categories: category };

    var options = {
      sort: { publishedDate: -1 },
      lean: true,
      page: parseInt(req.query.page) || 1,
      limit: 6,
      populate: 'categories'
    };

    return Post.model.paginate(query, options)
      .then((result) => {
        res.json(Object.assign({ filter: filter }, result));
      });
  });
}

export const getPostImages = (req, res, next) => {
  Post.model.find()
    .limit(7)
    .sort({ publishedDate: -1 })
    .select('heroImage slug title')
    .populate('categories')
    .lean()
    .exec()
    .then((posts) => {
      res.json(posts);
    });
}

export const getPost = (req, res, next) => {
  Post.model.findOne({ slug: req.params.slug })
    .populate('categories')
    .populate('author')
    .populate('place')
    .populate('places')
    .populate('newGallery1')
    .populate('newGallery2')
    .populate('newGallery3')
    .populate('listItems1')
    .populate('listItems2')
    .populate('listItems3')
    .populate('listItems4')
    .populate('listItems5')
    .lean()
    .exec()
    .then((post) => {
      res.json(post);
    });
}

export const getFeaturedPost = (req, res, next) => {
  Post.model.findOne({ featured: 'true' })
    .populate('categories')
    .populate('author')
    .populate('place')
    .populate('places')
    .lean()
    .exec()
    .then((post) => {
      res.json(post);
    });
}
