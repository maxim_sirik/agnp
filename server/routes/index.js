var webpack = require('webpack');
var webpackConfig = require('../../webpack/client.dev');
var compiler = webpack(webpackConfig);

var api = require('./api');

exports = module.exports = function (app) {
  if (process.env.NODE_ENV === 'development') {
    app.use(require("webpack-dev-middleware")(compiler, {
        noInfo: true, publicPath: webpackConfig.output.publicPath
    }));

    app.use(require("webpack-hot-middleware")(compiler, {
      log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
    }));
  }

  app.enable('trust proxy');

  if (process.env.NODE_ENV !== 'development') {
    app.use (function (req, res, next) {
      if (req.secure) {
        next();
      } else {
        res.redirect('https://' + req.headers.host + req.url);
      }
    });
  }

  app.use(require('prerender-node').set('prerenderToken', 'y7FKWLXTZ1EXTq1sU7Po'));

  app.use('/api', api);

  app.use(function (req, res) {
		res.render('index');
	});
};
