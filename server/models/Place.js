var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Post Model
 * ==========
 */

var Place = new keystone.List('Place', {
  map: { name: 'title' },
});

Place.add({
  title: { type: String, required: true, index: true },
  slug: {
    type: String,
    required: true,
    initial: true,
    unique: true,
  },
  state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
  publishedDate: { type: Types.Date, index: true, dependsOn: { state: 'published' } },
  image: { type: Types.CloudinaryImage },
  brief: String,
  tags: {
    type: Types.TextArray,
  },
  articleLink: String,
  location: { type: Types.Location, defaults: { country: 'USA' } },
  instagram: String,
  website: { type: Types.Url },
  hours: String,
  neighborhood: String,
  phone: String,
});

Place.defaultColumns = 'title, location, state|20%, author|20%, publishedDate|20%';
Place.register();
