var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Post Model
 * ==========
 */

var City = new keystone.List('City', {
  map: { name: 'title' },
});

City.add({
  title: { type: String, required: true },
  slug: {
    type: String,
    required: true,
    initial: true,
    unique: true,
  },
  state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
  publishedDate: { type: Types.Date, index: true, dependsOn: { state: 'published' } },
  tags: {
    type: Types.TextArray,
  },
  image: { type: Types.CloudinaryImage },
  location: { type: Types.Location, defaults: { country: 'USA' } },
  places: { type: Types.Relationship, ref: 'Place', many: true },
});

City.defaultColumns = 'title, location, places, state|20%, publishedDate|20%';
City.register();
