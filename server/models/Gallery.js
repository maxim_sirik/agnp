var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Gallery Model
 * ==========
 */

var Gallery = new keystone.List('Gallery', {
    label: 'Gallery',
    map: { name: 'title' },
});

Gallery.add({
    title: {
        type: String,
        required: true,
        initial: true,
        index: true
    },
    images: {
        type: Types.CloudinaryImages,
        note: 'Add 2 or 3 images',
        collapse: true
    }
});

Gallery.defaultColumns = 'title';
Gallery.register();