var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * AboutArticle Model
 * ==========
 */

var AboutArticle = new keystone.List('AboutArticle', {
    map: { name: 'brief' },
});

AboutArticle.add({
    logo: {
        image: {
            type: Types.CloudinaryImage,
        },
        svgCode: {
            type: String,
        },
        height40: {
            type: Types.Boolean
        }
    },
    brief: {
        type: String,
        required: true,
        initial: true,
    },
    heroImage: {
        image: {
            type: Types.CloudinaryImage,
            required: true,
            initial: true
        },
        caption: {
            type: String,
        },
        link: {
            type: Types.Url,
            required: true,
            initial: true
        }
    },
    url: {
        type: Types.Url,
        required: true,
        initial: true
    }
});

// AboutArticle.defaultColumns = 'id|20%, brief|80%';
AboutArticle.defaultColumns = 'state|20%, publishedDate|20%';
AboutArticle.register();