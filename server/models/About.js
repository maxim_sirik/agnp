var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * About Model
 * ==========
 */

var About = new keystone.List('About', {
    label: 'About'
});

About.add({
    video: {
        publicId1: {
            type: String,
            required: true,
            initial: true,
        },
        publicId2: {
            type: String,
            required: true,
            initial: true,
        },
    },
    quote: {
        type: String,
        required: true,
        initial: true,
    },
    aboutContent: {
        type: Types.Html,
        wysiwyg: true
    },
    signatureImage: {
        type: Types.CloudinaryImage
    },
    contactTitle: {
        type: String
    },
    ContactLink: {
        type: String,
    },
    ContactLinkLabel: {
        type: String,
    },
    articlesSectionTitle: {
        type: String,
    }
});

About.defaultColumns = 'state|20%, author|20%, publishedDate|20%';
About.register();