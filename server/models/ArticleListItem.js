var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ArticleListItem Model
 * ==========
 */

var ArticleListItem = new keystone.List('ArticleListItem', {
  label: 'ArticleListItem',
  map: { name: 'heading' },
});

ArticleListItem.add({
  heading: {
    type: String,
    required: true,
    initial: true,
    index: true
  },
  image: {
    type: Types.CloudinaryImage
  },
  content: {
    type: Types.Html,
    wysiwyg: true,
  },
  link: {
    type: String,
  }
});

ArticleListItem.defaultColumns = 'heading';
ArticleListItem.register();