var keystone = require('keystone');
var Types = keystone.Field.Types;

var mongoosePaginate = require('mongoose-paginate');


/**
 * Post Model
 * ==========
 */

var Post = new keystone.List('Post', {
  map: { name: 'title' },
});

Post.add({
  title: {
    type: String,
    required: true,
    initial: true,
  },
  slug: {
    type: String,
    required: true,
    initial: true,
    unique: true,
  },
  postType: {
    type: Types.Select,
    required: true,
    initial: true,
    options: ['longform', 'shortform'],
    default: 'base',
  },
  categories: {
    type: Types.Relationship,
    ref: 'PostCategory',
    many: true,
  },
  tags: {
    type: Types.TextArray,
  },
  featured: {
    type: Types.Select,
    required: true,
    options: 'true, false',
    default: 'false',
  },
  state: {
    type: Types.Select,
    required: true,
    options: 'draft, published, archived',
    default: 'draft',
  },
  publishedDate: {
    type: Types.Date,
    index: true,
    dependsOn: { state: 'published' }
  },
  author: {
    type: Types.Relationship,
    required: true,
    initial: true,
    ref: 'User', index: true
  },
  subheader: {
    type: String,
  },
  brief: {
    type: String,
  },
}, 'Body Content', {
  bodyContent: {
    body1: {
      type: Types.Html,
      wysiwyg: true,
      height: 200,
    },
    body2: {
      type: Types.Html,
      wysiwyg: true,
      height: 200,
    },
    body3: {
      type: Types.Html,
      wysiwyg: true,
      height: 200,
    },
    body4: {
      type: Types.Html,
      wysiwyg: true,
      height: 200,
    },
    body5: {
      type: Types.Html,
      wysiwyg: true,
      height: 200,
    },
  },
}, 'Hero Image', {
  heroImage: {
    image: {
      type: Types.CloudinaryImage,
    },
    caption: {
      type: String,
    },
    link: {
      type: Types.Url,
    },
  },
}, 'Image Galleries', {
    newGallery1: {
        type: Types.Relationship,
        ref: 'Gallery',
        many: false,
    },
    gallery1fullWidth: {
        label: 'Full width images (gallery 1)',
        type: Types.Boolean,
        default: false
    },
    newGallery2: {
        type: Types.Relationship,
        ref: 'Gallery',
        many: false,
    },
    gallery2fullWidth: {
        label: 'Full width images (gallery 2)',
        type: Types.Boolean,
        default: false
    },
    newGallery3: {
        type: Types.Relationship,
        ref: 'Gallery',
        many: false,
    },
    gallery3fullWidth: {
        label: 'Full width images (gallery 3)',
        type: Types.Boolean,
        default: false
    }
}, 'Places', {
  place: {
    type: Types.Relationship,
    dependsOn: { postType: 'shortform' },
    ref: 'Place',
    many: false,
  },
  placesTitle: {
    type: String,
    dependsOn: { postType: 'longform' },
  },
  places: {
    type: Types.Relationship,
    dependsOn: { postType: 'longform' },
    ref: 'Place',
    many: true,
  },
}, 'List', {
  type2: {
    label: 'Type',
    type: Types.Select,
    options: ['numbered-stacked', 'not-numbered-stacked', 'numbered-alternating', 'not-numbered-alternating'],
  },
  listDirection2: {
    label: 'Direction',
    type: Types.Select,
    dependsOn: { type2: ['numbered-alternating', 'not-numbered-alternating'] },
    options: ['left-right', 'right-left'],
  },
  list1Title: {
    type: String,
  },
  listItems1: {
    type: Types.Relationship,
    ref: 'ArticleListItem',
    many: true,
  },
  list2Title: {
    type: String,
  },
  listItems2: {
    type: Types.Relationship,
    ref: 'ArticleListItem',
    many: true,
  },
  list3Title: {
    type: String,
  },
  listItems3: {
    type: Types.Relationship,
    ref: 'ArticleListItem',
    many: true,
  },
  list4Title: {
    type: String,
  },
  listItems4: {
    type: Types.Relationship,
    ref: 'ArticleListItem',
    many: true,
  },
  list5Title: {
    type: String,
  },
  listItems5: {
    type: Types.Relationship,
    ref: 'ArticleListItem',
    many: true,
  },
}, 'Image Link', {
  imageLink: {
    title: String,
    url: Types.Url,
    image: {
      type: Types.CloudinaryImage,
    },
  }
}, 'Image Gallery (Old page)', {
  gallery1: {
    type: Types.CloudinaryImages,
    note: 'Add 2 or 3 images',
  },
  gallery2: {
    type: Types.CloudinaryImages,
    note: 'Add 2 or 3 images',
    collapse: true,
  },
  gallery3: {
    type: Types.CloudinaryImages,
    note: 'Add 2 or 3 images',
    collapse: true,
  },
}, 'List (Old page)', {
  articleList: {
    title: {
      type: String,
    },
    type: {
      type: Types.Select,
      options: ['numbered', 'not-numbered'],
    },
    items: {
      item1: {
        heading: String,
        content: String,
      },
      item2: {
        heading: String,
        content: String,
      },
      item3: {
        heading: String,
        content: String,
      },
      item4: {
        heading: String,
        content: String,
      },
      item5: {
        heading: String,
        content: String,
      },
      item6: {
        heading: String,
        content: String,
      },
      item7: {
        heading: String,
        content: String,
      },
      item8: {
        heading: String,
        content: String,
      },
      item9: {
        heading: String,
        content: String,
      },
      item10: {
        heading: String,
        content: String,
      },
    },
  },
});

Post.schema.virtual('content.full').get(function () {
  return this.content.extended || this.content.brief;
});

Post.schema.plugin(mongoosePaginate);

Post.defaultColumns = 'title, featured, state|20%, author|20%, publishedDate|20%';
Post.register();
