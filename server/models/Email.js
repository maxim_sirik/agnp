var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Enquiry Model
 * =============
 */

var Email = new keystone.List('Email');

Email.add({
  email: { type: Types.Email, initial: true, required: true, unique: true },
  createdAt: { type: Date, default: Date.now },
});

Email.defaultSort = '-createdAt';
Email.defaultColumns = 'email, createdAt';
Email.register();
