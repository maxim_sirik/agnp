import { combineReducers } from 'redux';
import headerReducer from '../components/header/header.reducer';
import articlesReducer from '../components/articles/articles.reducer';
import mapsReducer from '../components/maps/maps.reducer';
import searchReducer from '../components/search/search.reducer';
import footerReducer from '../components/footer/footer.reducer';
import aboutReducer from '../components/about/about.reducer';

const rootReducer = combineReducers({
  headerReducer,
  articlesReducer,
  mapsReducer,
  searchReducer,
  footerReducer,
  aboutReducer,
});

export default rootReducer;
