import React from 'react';

export default () => (
    <svg width="27px" height="26px" viewBox="0 0 27 26" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs></defs>
    <g id="Instagram-Map-DESKTOP" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-1222.000000, -145.000000)" stroke-linecap="round" stroke-linejoin="round">
        <g id="Map-NAV" transform="translate(1196.000000, 120.000000)" stroke="#B8B8B8" stroke-width="1.73107179">
            <g id="city-guide-logo" transform="translate(27.000000, 26.000000)">
                <polygon id="Stroke-1" points="8.33333333 24 0 18.2857143 0 0 8.33333333 5.71428571 16.6666667 0 25 5.71428571 25 24 16.6666667 18.2857143"></polygon>
                <path d="M8.33333333,6 L8.33333333,24" id="Stroke-3"></path>
                <path d="M16.6666667,18 L16.6666667,0" id="Stroke-5"></path>
            </g>
        </g>
    </g>
</svg>
);