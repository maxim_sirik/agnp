import React from 'react';

export default () => (
    <svg width="22px" height="22px" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs></defs>
    <g id="Single-Entry-Multi-Images-DESKTOP" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-1655.000000, -1921.000000)" stroke-linecap="round" stroke-linejoin="round">
        <g id="Item-Details" transform="translate(0.000000, 563.000000)" stroke="#4F4F4F" stroke-width="0.85">
            <g id="Map-&amp;-Details" transform="translate(375.000000, 1076.000000)">
                <g id="Hours" transform="translate(1281.000000, 283.000000)">
                    <g id="Hours-Icon_desktop">
                        <path d="M19.5833333,10 C19.5833333,15.2933333 15.2916667,19.5833333 10,19.5833333 C4.705,19.5833333 0.416666667,15.2933333 0.416666667,10 C0.416666667,4.70666667 4.705,0.416666667 10,0.416666667 C15.2916667,0.416666667 19.5833333,4.70666667 19.5833333,10 Z" id="Stroke-1"></path>
                        <path d="M15,5 L10.8825,9.11916667" id="Stroke-3"></path>
                        <path d="M10,8.75 C10.69,8.75 11.25,9.31166667 11.25,10 C11.25,10.6916667 10.69,11.25 10,11.25 C9.31,11.25 8.75,10.6916667 8.75,10 C8.75,9.31166667 9.31,8.75 10,8.75 Z" id="Stroke-5"></path>
                        <path d="M5.41666667,10.4166667 L8.81916667,10.4166667" id="Stroke-7"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
);