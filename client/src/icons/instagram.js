import React from 'react';

export default () => (
    <svg width="17px" height="17px" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <polygon id="path-1" points="0 16.3178631 16.3178631 16.3178631 16.3178631 0 0 0"></polygon>
    </defs>
    <g id="City-Guide-Map-DESKTOP" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-1538.000000, -1254.000000)">
        <g id="map-card" transform="translate(665.000000, 1040.000000)">
            <g id="Instagram" transform="translate(873.000000, 214.000000)">
                <g id="instagram-logo_desktop">
                    <path d="M15.9779076,13.2582638 C15.9779076,14.7601871 14.7601871,15.9779076 13.2582638,15.9779076 L3.05959933,15.9779076 C1.55767602,15.9779076 0.339955481,14.7601871 0.339955481,13.2582638 L0.339955481,3.05959933 C0.339955481,1.55767602 1.55767602,0.339955481 3.05959933,0.339955481 L13.2582638,0.339955481 C14.7601871,0.339955481 15.9779076,1.55767602 15.9779076,3.05959933 L15.9779076,13.2582638 Z" id="Stroke-1" stroke="#000000" stroke-width="0.678984728" stroke-linejoin="round"></path>
                    <path d="M0.339955481,5.77924318 L5.23871397,5.77924318" id="Stroke-3" stroke="#000000" stroke-width="0.678984728" stroke-linejoin="round"></path>
                    <path d="M11.0618114,5.77924318 L15.9945654,5.77924318" id="Stroke-5" stroke="#000000" stroke-width="0.678984728" stroke-linejoin="round"></path>
                    <path d="M11.8984418,8.15893155 C11.8984418,10.2245011 10.2238211,11.8984418 8.15893155,11.8984418 C6.09404196,11.8984418 4.41942126,10.2245011 4.41942126,8.15893155 C4.41942126,6.09336205 6.09404196,4.41942126 8.15893155,4.41942126 C10.2238211,4.41942126 11.8984418,6.09336205 11.8984418,8.15893155 Z" id="Stroke-7" stroke="#000000" stroke-width="0.678984728" stroke-linejoin="round"></path>
                    <mask id="mask-2" fill="white">
                        <use href="#path-1"></use>
                    </mask>
                    <g id="Clip-10"></g>
                    <polygon id="Stroke-9" stroke="#000000" stroke-width="0.678984728" stroke-linejoin="round" mask="url(#mask-2)" points="12.5783528 4.41942126 14.6180857 4.41942126 14.6180857 2.37968837 12.5783528 2.37968837"></polygon>
                    <path d="M1.69977741,0.679910963 L1.69977741,5.77924318" id="Stroke-11" stroke="#000000" stroke-width="0.678984728" stroke-linejoin="round" mask="url(#mask-2)"></path>
                    <path d="M3.05959933,0.339955481 L3.05959933,5.77924318" id="Stroke-12" stroke="#000000" stroke-width="0.678984728" stroke-linejoin="round" mask="url(#mask-2)"></path>
                    <path d="M4.41942126,0.339955481 L4.41942126,5.77924318" id="Stroke-13" stroke="#000000" stroke-width="0.678984728" stroke-linejoin="round" mask="url(#mask-2)"></path>
                </g>
            </g>
        </g>
    </g>
</svg>
);