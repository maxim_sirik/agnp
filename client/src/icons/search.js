import React from 'react';

export default () => (
    <svg width="28px" height="28px" viewBox="0 0 28 28" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs></defs>
    <g id="Nav-tablet" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-714.000000, -28.000000)" stroke-linejoin="round">
        <g id="search-icon" transform="translate(715.000000, 29.000000)" stroke="#4F4F4F" stroke-width="1.71449993">
            <path d="M17.9636364,8.98181818 C17.9636364,13.940732 13.941734,17.9636364 8.98117261,17.9636364 C4.02190234,17.9636364 0,13.940732 0,8.98181818 C0,4.02161329 4.02190234,0 8.98117261,0 C13.941734,0 17.9636364,4.02161329 17.9636364,8.98181818 Z" id="Stroke-1"></path>
            <path d="M15.1272727,15.1272727 L25.5272727,25.5272727" id="Stroke-3" stroke-linecap="round"></path>
        </g>
    </g>
</svg>
);