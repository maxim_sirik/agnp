import React from 'react';

export default () => (
  <svg width="18px" height="17px" viewBox="0 0 18 17" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs></defs>
    <g id="City-Guide-Map-DESKTOP" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-1537.000000, -1283.000000)" stroke-linejoin="round">
        <g id="map-card" transform="translate(665.000000, 1040.000000)" stroke="#000000" stroke-width="0.678984728">
            <g id="website" transform="translate(873.000000, 243.000000)">
                <g id="globe-icon_desktop" transform="translate(0.000000, 0.400005)">
                    <path d="M15.8228428,8.05012377 C15.8228428,12.3424907 12.3465306,15.8228428 8.05820352,15.8228428 C3.76987646,15.8228428 0.33665623,12.2037883 0.33665623,7.91074809 C0.33665623,3.73149765 3.61568791,0.510370845 7.74645985,0.344062667 C7.85822972,0.338676167 7.96999959,0.33665623 8.08311608,0.33665623 C12.3714431,0.33665623 15.8228428,3.75775684 15.8228428,8.05012377 Z" id="Stroke-1"></path>
                    <path d="M7.74639252,0.343860673 C3.70651776,4.72039166 3.70651776,10.4300813 7.74639252,15.816581" id="Stroke-3"></path>
                    <path d="M8.41970498,0.343860673 C12.4595797,4.72039166 12.4595797,10.4280614 8.41970498,15.8145611" id="Stroke-5"></path>
                    <path d="M1.60665819,3.70288187 L14.4898188,3.70288187" id="Stroke-7"></path>
                    <path d="M0.338945492,7.74275663 L15.8231121,7.74275663" id="Stroke-9"></path>
                    <path d="M1.33679456,11.7826314 L14.870375,11.7826314" id="Stroke-11"></path>
                </g>
            </g>
        </g>
    </g>
  </svg>
);