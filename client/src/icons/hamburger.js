import React from 'react';

export default () => (
    <svg width="29px" height="17px" viewBox="0 0 29 17" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs></defs>
    <g id="Nav-tablet" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-27.000000, -34.000000)" stroke-linecap="round">
        <g id="hamburger" transform="translate(28.000000, 34.000000)" stroke="#4F4F4F" stroke-width="1.79999995">
            <path d="M0.642857143,1.5 L26.3892657,1.5" id="Line"></path>
            <path d="M0.642857143,8.5 L26.3892657,8.5" id="Line"></path>
            <path d="M0.642857143,15.5 L26.3892657,15.5" id="Line"></path>
        </g>
    </g>
</svg>
);