import React from 'react';

export default () => (
  <svg width="22px" height="18px" viewBox="0 0 22 18" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs></defs>
    <g id="Single-Entry-Multi-Images-DESKTOP" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-1655.000000, -2007.000000)" stroke-linecap="round" stroke-linejoin="round">
        <g id="Item-Details" transform="translate(0.000000, 563.000000)" stroke="#4F4F4F" stroke-width="0.849999964">
            <g id="Map-&amp;-Details" transform="translate(375.000000, 1076.000000)">
                <g id="Neighborhood" transform="translate(1281.000000, 366.000000)">
                    <g id="Map-Icon_desktop" transform="translate(0.000000, 2.000000)">
                        <polygon id="Stroke-1" points="0.416666667 16.2857143 5.41666667 17.5714286 10 16.2857143 14.5833333 17.5714286 19.5833333 16.2857143 18.75 0.428571429 14.1666667 1.71428571 10 0.428571429 5.83333333 1.71428571 1.25 0.428571429"></polygon>
                        <path d="M14.1666667,4.71428571 L15.8333333,6.42857143" id="Stroke-3"></path>
                        <path d="M14.1666667,6.42857143 L15.8333333,4.71428571" id="Stroke-4"></path>
                        <path d="M3.75,12.4285714 L4.16666667,12" id="Stroke-5"></path>
                        <path d="M6.25,9.85714286 L6.66666667,9.42857143" id="Stroke-6"></path>
                        <path d="M15.4166667,7.71428571 L15.4166667,8.57142857" id="Stroke-7"></path>
                        <path d="M11.6666667,9.85714286 L12.0833333,10.2857143" id="Stroke-8"></path>
                        <path d="M9.16666667,8.14285714 L10,8.14285714" id="Stroke-9"></path>
                        <path d="M14.5833333,11.1428571 L15,10.7142857" id="Stroke-10"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
);