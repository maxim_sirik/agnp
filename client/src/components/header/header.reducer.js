import { handleActions } from 'redux-actions';
import {
  toggleMobileTakeover,
} from './header.actions';

const initialState = {
  displayMobileTakeover: false,
};

export default handleActions(
  {
    [toggleMobileTakeover]: (state, { payload: { value } }) => ({
      ...state,
      displayMobileTakeover: value,
    }),
  },
  initialState,
);
