import React, { Component } from 'react';
import MediaQuery from 'react-responsive';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { withRouter } from 'react-router'
import zenscroll from 'zenscroll';

import Search from '../search';
import SearchIcon from '../../icons/search';
import MobileMenuIcon from '../../icons/hamburger';
import { toggleSearch } from '../search/search.actions';
import { toggleMobileTakeover } from './header.actions';

const mapDispatchToProps = dispatch => ({
  onToggleSearch: value => dispatch(toggleSearch(value)),
  onToggleMobileTakeover: value => dispatch(toggleMobileTakeover(value)),
});

const mapStateToProps = (state) => ({
  displayMobileTakeover: state.rootReducer.headerReducer.displayMobileTakeover,
  displaySearch: state.rootReducer.searchReducer.displaySearch,
  onMapsLandingPage: !!state.router.location.pathname.match(/\/maps-landing/),
  onAboutPage: !!state.router.location.pathname.match(/\/about/),
  onArticlesPage: !!state.router.location.pathname.match(/\/articles/),
  onHomePage: !!state.router.location.pathname.match(/\/$/),
});

class Header extends Component {
  constructor(props) {
    super(props);

    this.onSearchClick = this.onSearchClick.bind(this);
    this.onMobileMenuClick = this.onMobileMenuClick.bind(this);
    this.onMobileTakeoverCloseCLick = this.onMobileTakeoverCloseCLick.bind(this);
    this.onScrollAnimations = this.onScrollAnimations.bind(this);

    this.state = {
      shrinkHeader: window.scrollY > 50,
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScrollAnimations);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScrollAnimations);
  }

  onSearchClick() {
    this.props.onToggleSearch(!this.props.displaySearch);
  }

  onMobileMenuClick() {
    this.props.onToggleMobileTakeover(true);
  }

  onMobileTakeoverCloseCLick() {
    this.props.onToggleMobileTakeover(false);
  }

  onScrollAnimations() {
    const { location: { pathname } } = this.props;
    const onMapsPage = !!pathname.match(/\/maps($|\/)/);

    if (onMapsPage) return;

    return window.scrollY > 50
      ? this.header.classList.add('shrink-header')
      : this.header.classList.remove('shrink-header');
  }

  renderLogo() {
    const { onHomePage } = this.props;

    if (onHomePage) {
      return (
        <span className="header-link" onClick={() =>{
          var title = document.getElementById("title")
          zenscroll.center(title);
        }}>
          <svg xmlns="http://www.w3.org/2000/svg" width="1em" viewBox="0 0 48 17">
            <path fill-rule="evenodd" d="M1.965 8.93c0 1.085.685 1.74 1.896 1.74.868 0 1.622-.271 2.262-.836.64-.565.96-1.447.96-2.645v-.271H5.025c-1.942 0-3.061.723-3.061 2.012zM9 4.928v7.28H7.494l-.32-1.606c-.846 1.199-2.034 1.786-3.61 1.786C1.485 12.388 0 11.258 0 8.93 0 6.51 1.988 5.2 5.209 5.2h1.873v-.498c0-1.56-.845-2.35-2.513-2.35-1.21 0-2.147.677-2.399 1.785L.48 3.753C.777 1.83 2.399.633 4.57.633 7.241.633 9 2.035 9 4.928zm3.793-.452c0-1.153.388-2.057 1.142-2.758.754-.7 1.714-1.062 2.879-1.062.754 0 1.44.158 2.079.497L20.95 0l.868 1.56-1.553.859c.388.588.57 1.266.57 2.057 0 1.153-.388 2.08-1.142 2.78-.753.679-1.713 1.018-2.878 1.018-.754 0-1.417-.136-1.988-.407-.502.249-.754.61-.754 1.108 0 .633.434.95 1.303.95h2.718c2.193 0 3.61 1.13 3.61 3.141 0 2.645-2.033 3.934-4.866 3.934-2.947 0-4.89-1.402-4.89-3.753 0-.972.366-1.763 1.074-2.373-.525-.43-.776-1.04-.776-1.809 0-.927.457-1.627 1.393-2.102-.57-.678-.845-1.515-.845-2.487zm5.003 7.076h-2.33c-.229 0-.503-.023-.845-.045-.503.384-.754.95-.754 1.673 0 1.288 1.05 2.147 2.97 2.147 1.919 0 2.901-.836 2.901-2.08 0-1.13-.64-1.695-1.942-1.695zM15.284 2.87c-.411.384-.617.927-.617 1.605 0 .678.206 1.22.617 1.605.823.746 2.24.746 3.062 0 .41-.384.616-.927.616-1.605 0-.678-.205-1.22-.616-1.605-.823-.791-2.24-.791-3.062 0zm18.254 1.311v8.025H31.62V4.68c0-1.56-.662-2.329-1.987-2.329-.891 0-1.668.475-2.33 1.402-.64.927-.96 2.17-.96 3.684v4.77h-1.92V.814h1.92v2.102C27.21 1.402 28.489.633 30.157.633c2.102 0 3.381 1.289 3.381 3.55zm9.344 8.206c-.685 0-1.37-.158-2.033-.497-.662-.34-1.12-.678-1.325-1.04v5.968h-1.919V.814h1.919v1.401c.571-.881 1.988-1.582 3.358-1.582C46.172.633 48 2.984 48 6.511c0 3.526-1.828 5.877-5.118 5.877zM42.7 2.351c-2.08 0-3.336 1.696-3.336 4.16 0 2.464 1.257 4.16 3.336 4.16 2.01 0 3.335-1.696 3.335-4.16 0-2.464-1.325-4.16-3.335-4.16z"/>
          </svg>
        </span>
      );
    } else {
      return (
        <Link className="header-link" to="/">
          <svg xmlns="http://www.w3.org/2000/svg" width="1em" viewBox="0 0 48 17">
            <path fill-rule="evenodd" d="M1.965 8.93c0 1.085.685 1.74 1.896 1.74.868 0 1.622-.271 2.262-.836.64-.565.96-1.447.96-2.645v-.271H5.025c-1.942 0-3.061.723-3.061 2.012zM9 4.928v7.28H7.494l-.32-1.606c-.846 1.199-2.034 1.786-3.61 1.786C1.485 12.388 0 11.258 0 8.93 0 6.51 1.988 5.2 5.209 5.2h1.873v-.498c0-1.56-.845-2.35-2.513-2.35-1.21 0-2.147.677-2.399 1.785L.48 3.753C.777 1.83 2.399.633 4.57.633 7.241.633 9 2.035 9 4.928zm3.793-.452c0-1.153.388-2.057 1.142-2.758.754-.7 1.714-1.062 2.879-1.062.754 0 1.44.158 2.079.497L20.95 0l.868 1.56-1.553.859c.388.588.57 1.266.57 2.057 0 1.153-.388 2.08-1.142 2.78-.753.679-1.713 1.018-2.878 1.018-.754 0-1.417-.136-1.988-.407-.502.249-.754.61-.754 1.108 0 .633.434.95 1.303.95h2.718c2.193 0 3.61 1.13 3.61 3.141 0 2.645-2.033 3.934-4.866 3.934-2.947 0-4.89-1.402-4.89-3.753 0-.972.366-1.763 1.074-2.373-.525-.43-.776-1.04-.776-1.809 0-.927.457-1.627 1.393-2.102-.57-.678-.845-1.515-.845-2.487zm5.003 7.076h-2.33c-.229 0-.503-.023-.845-.045-.503.384-.754.95-.754 1.673 0 1.288 1.05 2.147 2.97 2.147 1.919 0 2.901-.836 2.901-2.08 0-1.13-.64-1.695-1.942-1.695zM15.284 2.87c-.411.384-.617.927-.617 1.605 0 .678.206 1.22.617 1.605.823.746 2.24.746 3.062 0 .41-.384.616-.927.616-1.605 0-.678-.205-1.22-.616-1.605-.823-.791-2.24-.791-3.062 0zm18.254 1.311v8.025H31.62V4.68c0-1.56-.662-2.329-1.987-2.329-.891 0-1.668.475-2.33 1.402-.64.927-.96 2.17-.96 3.684v4.77h-1.92V.814h1.92v2.102C27.21 1.402 28.489.633 30.157.633c2.102 0 3.381 1.289 3.381 3.55zm9.344 8.206c-.685 0-1.37-.158-2.033-.497-.662-.34-1.12-.678-1.325-1.04v5.968h-1.919V.814h1.919v1.401c.571-.881 1.988-1.582 3.358-1.582C46.172.633 48 2.984 48 6.511c0 3.526-1.828 5.877-5.118 5.877zM42.7 2.351c-2.08 0-3.336 1.696-3.336 4.16 0 2.464 1.257 4.16 3.336 4.16 2.01 0 3.335-1.696 3.335-4.16 0-2.464-1.325-4.16-3.335-4.16z"/>
          </svg>
        </Link>
      );
    }
  }

  render() {
    const {
      location: { pathname },
      onHomePage,
      onAboutPage,
      onArticlesPage,
      onMapsLandingPage
    } = this.props;
    const onMapsPage = !!pathname.match(/\/maps($|\/)/);

    return (
      <div>
        <div className={classNames('mobile-takeover', { show: this.props.displayMobileTakeover })}>
          <div className="mobile-takeover-header">
            <span
              className="mobile-takeover-close"
              onClick={this.onMobileTakeoverCloseCLick}>
              X
            </span>
            <SearchIcon />
          </div>
          <div className="mobile-takeover-links">
            <Link onClick={this.onMobileTakeoverCloseCLick} to="/about">
              <p className="nav-title">ABOUT</p>
            </Link>
            <Link onClick={this.onMobileTakeoverCloseCLick} to="/maps">
              <p className="nav-title">MAP</p>
            </Link>
            <Link onClick={this.onMobileTakeoverCloseCLick} to="/articles">
              <p className="nav-title">ARTICLES</p>
            </Link>
            <a onClick={this.onMobileTakeoverCloseCLick} href="https://www.studio.agnp.co/">
              <p className="nav-title">SHOP</p>
            </a>
          </div>
        </div>
        <div
          className={classNames('header', { 'shrink-header': onMapsPage })}
          ref={(ref) => this.header = ref}
        >
          <MediaQuery maxWidth={768}>
            <div className="mobile-menu" onClick={this.onMobileMenuClick}>
              <MobileMenuIcon />
            </div>
          </MediaQuery>
          {this.renderLogo()}
          <MediaQuery minWidth={1100}>
            <div className={classNames('header-nav-links', { hide: onHomePage })}>
              <Link to="/about">
                <p className={classNames('nav-title', {'current-active-page': onAboutPage })}>
                  ABOUT
                </p>
              </Link>
              <Link to="/maps">
                <p className={classNames('nav-title', {'current-active-page': onMapsLandingPage })}>
                  MAP
                </p>
              </Link>
              <Link to="/articles">
                <p className={classNames('nav-title', {'current-active-page': onArticlesPage })}>
                  ARTICLES
                </p>
              </Link>
            </div>
          </MediaQuery>
          <button className="search-button" onClick={this.onSearchClick}>
            <SearchIcon />
          </button>
        </div>
        <Search />
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
