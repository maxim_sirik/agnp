import { get } from 'axios';
import { createActions } from 'redux-actions';

export const {
  toggleMobileTakeover,
} = createActions({
  TOGGLE_MOBILE_TAKEOVER: value => ({ value }),
});
