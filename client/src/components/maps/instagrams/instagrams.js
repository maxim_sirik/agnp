import React, { Component } from 'react';

import { INSTAGRAM_TYPE } from '../maps.constants';

class Instagrams extends Component {
  componentWillMount() {
    const { getInstagrams, onChangeMapType } = this.props;

    getInstagrams().then(() => {
      onChangeMapType(INSTAGRAM_TYPE, false);
    });
  }

  render() {
    return (
      <div>
      </div>
    );
  }
}

export default Instagrams;
