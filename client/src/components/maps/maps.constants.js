export const MAP_TYPE = 'MAP';
export const CITY_TYPE = 'CITY';
export const INSTAGRAM_TYPE = 'INSTAGRAM';
export const ARTICLE_HERO_TYPE = 'ARTICLE_HERO';
export const ARTICLE_LIST_TYPE = 'ARTICLE_LIST';

export const mapRoutes = {
  MAP: '/maps',
  CITY: '/maps/city',
  INSTAGRAM: '/maps/instagram',
};
