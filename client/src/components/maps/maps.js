import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';

import MapContainer from '../map-container';
import City from './city';

import { MAP_TYPE, CITY_TYPE, INSTAGRAM_TYPE } from './maps.constants';

const calculateMapHeight = (mapType) => {
  const width = window.innerWidth || screen.width;
  const height = window.innerHeight || screen.height;
  const bannerHeight = 60;

  if (mapType === CITY_TYPE) {
    if (width <= 414) {
      return (height - bannerHeight) * 0.48;
    } else if (width <= 768) {
      return (height - bannerHeight) * 0.68;
    }
  }

  if (width <= 414 && height >= 800) {
    return height + 20 - bannerHeight;
  }

  return height - bannerHeight;
}

const mapLocation = (pathname) => {
  if (location.pathname.match(/\/maps$/)) {
    return MAP_TYPE;
  } else if (location.pathname.match(/\/maps\/instagrams$/)) {
    return INSTAGRAM_TYPE;
  } else {
    return CITY_TYPE;
  }
}

class Maps extends Component {
  componentWillMount() {
    const {
      onChangeMapType,
      mapType,
      getMaps,
      getInstagrams,
      location,
    } = this.props;

    if (mapLocation(location.pathname) === MAP_TYPE) {
      getMaps();
      onChangeMapType(MAP_TYPE);
    } else {
      onChangeMapType(CITY_TYPE);
    }

    this.props.history.listen((location, action) => {
      if (mapLocation(location.pathname) === MAP_TYPE) {
        getMaps();
        onChangeMapType(MAP_TYPE);
      } else if (mapLocation(location.pathname) === INSTAGRAM_TYPE) {
        getInstagrams();
        onChangeMapType(INSTAGRAM_TYPE);
      }
    });
  }

  render() {
    const {
      markers,
      focussedMarker,
      animate,
      resetView,
      zoom,
      mapType,
    } = this.props;

    return (
      <div className="map-container">
        <div className="map-toggle-links-container">
          <div className="map-toggle-links">
            <Link to="/maps">
              <svg width="27px" height="26px" viewBox="0 0 27 26" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs></defs>
                <g id="Instagram-Map-DESKTOP" stroke="none" stroke-width="1" fill="white" fill-rule="evenodd" transform="translate(-1222.000000, -145.000000)" stroke-linecap="round" stroke-linejoin="round">
                  <g id="Map-NAV" transform="translate(1196.000000, 120.000000)" stroke={mapType === MAP_TYPE || mapType === CITY_TYPE ? '#333333' : '#cccccc'} stroke-width="1.73107179">
                    <g id="city-guide-logo" transform="translate(27.000000, 26.000000)">
                      <polygon id="Stroke-1" points="8.33333333 24 0 18.2857143 0 0 8.33333333 5.71428571 16.6666667 0 25 5.71428571 25 24 16.6666667 18.2857143"></polygon>
                      <path d="M8.33333333,6 L8.33333333,24" id="Stroke-3"></path>
                      <path d="M16.6666667,18 L16.6666667,0" id="Stroke-5"></path>
                    </g>
                  </g>
                </g>
              </svg>
            </Link>
          </div>
        </div>
        <MapContainer
          focussedMarker={focussedMarker}
          markers={markers}
          animate={animate}
          resetView={resetView}
          zoom={zoom}
          height={calculateMapHeight(mapType)}
          mapType={mapType}
        />

        <Switch>
          <Route path="/maps/:slug" component={City} />
        </Switch>
      </div>
    );
  }
};

export default Maps;
