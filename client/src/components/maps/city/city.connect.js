import { connect } from 'react-redux';
import findIndex from 'lodash/findIndex';

import City from './city';

import { fetchMap, changeMapType, goBackPlace, goForwardPlace } from '../maps.actions';


/*
This is a redux specific function.
What is does is: It gets the state specified in here from the global redux state.
For example, here we are retrieving the list of items from the redux store.
Whenever this list changes, any component that is using this list of item will re-render.
 */
const mapStateToProps = state => ({
  city: state.rootReducer.mapsReducer.currentPlace,
  markersTotal: state.rootReducer.mapsReducer.markers && state.rootReducer.mapsReducer.markers.length,
  focussedMarker: state.rootReducer.mapsReducer.focussedMarker,
  focussedMarkerIndex: findIndex(state.rootReducer.mapsReducer.markers,
    (marker) => marker._id === state.rootReducer.mapsReducer.focussedMarker._id) + 1,
});

/*
This is a redux specific function.
http://redux.js.org/docs/api/bindActionCreators.html
 */
const mapDispatchToProps = dispatch => ({
  getMap: slug => dispatch(fetchMap(slug)),
  onChangeMapType: type => dispatch(changeMapType(type)),
  onClickGoBackPlace: () => dispatch(goBackPlace()),
  onClickGoForwardPlace: () => dispatch(goForwardPlace()),
});

/*
Here we are creating a Higher order component
https://facebook.github.io/react/docs/higher-order-components.html
 */
export default connect(mapStateToProps, mapDispatchToProps)(City);
