import React, { Component } from 'react';
import ProgressiveImage from 'react-progressive-image';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import MediaQuery from 'react-responsive';
import { Image, Transformation } from 'cloudinary-react';
import urlParse from 'url-parse';

import NavLeftIcon from 'react-icons/lib/md/keyboard-arrow-left';
import NavRightIcon from 'react-icons/lib/md/keyboard-arrow-right';
import MapMarkerIcon from 'react-icons/lib/fa/map-marker';

import InstagramIcon from '../../../icons/instagram';
import GlobeIcon from '../../../icons/globe';
import OkIcon from '../../../icons/ok';

import { CITY_TYPE } from '../maps.constants';

class City extends Component {
  componentWillMount() {
    const { getMap, onChangeMapType, match: { params: { slug } } } = this.props;

    getMap(slug).then(() => {
      onChangeMapType(CITY_TYPE);
    });
  }

  componentWillReceiveProps(nextProps) {
    const { getMap, onChangeMapType, match: { params: { slug } } } = this.props;
    const nextSlug = get(nextProps, 'match.params.slug');

    if (nextSlug !== slug) {
      getMap(nextSlug).then(() => {
        onChangeMapType(CITY_TYPE);
      });
    }
  }

  shortenedWebsite(website) {
    if (!website) return;
    const linkElement = document.createElement("a");
    linkElement.href = website;
    return linkElement.hostname;
  }

  render() {
    const {
      city: { title },
      markersTotal,
      focussedMarker,
      focussedMarkerIndex,
      onClickGoBackPlace,
      onClickGoForwardPlace,
    } = this.props;

    return (
      <div className="city-places">
        <div className="city-places-header">
          <h2 className="city-title">{title}</h2>
          <span className="city-index">
            <span className="initial-number">{focussedMarkerIndex}</span>/{markersTotal}
          </span>
          <MediaQuery maxWidth={415}>
            <div className="city-mobile-nav">
              <svg
                onClick={onClickGoBackPlace}
                className="city-nav"
                viewBox="0 0 32 56"
              >
                  <path fill="#B8B8B8" fill-rule="evenodd" d="M32 56L0 28.467v-.934L32 0v2.333L1.803 28 32 53.667z"/>
              </svg>
              <svg
                onClick={onClickGoForwardPlace}
                className="city-nav"
                viewBox="0 0 32 56"
              >
                <path fill="#B8B8B8" fill-rule="evenodd" d="M0 0l32 27.533v.934L0 56v-2.333L30.197 28 0 2.333z"/>
              </svg>
            </div>
          </MediaQuery>
        </div>

        <MediaQuery minWidth={415}>
          <div className="city-fullsize-nav">
            <svg
              onClick={onClickGoBackPlace}
              className="city-nav"
              viewBox="0 0 32 56"
            >
                <path fill="#B8B8B8" fill-rule="evenodd" d="M32 56L0 28.467v-.934L32 0v2.333L1.803 28 32 53.667z"/>
            </svg>
          </div>
        </MediaQuery>

        {!isEmpty(focussedMarker) &&
          <div className="focussed-marker">
            <div className="focussed-marker-image">
              {focussedMarker.image &&
                <div>
                  <MediaQuery maxWidth={767}>
                    <ProgressiveImage
                      src={`https://res.cloudinary.com/agnp/image/upload/ar_0.65,c_crop,g_south,dpr_auto,f_auto,q_auto/c_scale/${focussedMarker.image.public_id}`}
                      placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_0.65,c_crop,g_south,dpr_auto,f_auto,q_10/c_scale/${focussedMarker.image.public_id}`}
                    >
                      {(src, loading) => (
                        <img style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
                      )}
                    </ProgressiveImage>
                  </MediaQuery>
                  <MediaQuery minWidth={768}>
                    <ProgressiveImage
                      src={`https://res.cloudinary.com/agnp/image/upload/ar_0.8,c_crop,g_south,dpr_auto,f_auto,q_auto/c_scale/${focussedMarker.image.public_id}`}
                      placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_0.8,c_crop,g_south,dpr_auto,f_auto,q_10/c_scale/${focussedMarker.image.public_id}`}
                    >
                      {(src, loading) => (
                        <img style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
                      )}
                    </ProgressiveImage>
                  </MediaQuery>
                </div>
              }
            </div>
            <div className="focussed-marker-info">
              <h3>{focussedMarker.title}</h3>
              <div className="address">
                <MapMarkerIcon />
                <div className="address-section-container">
                  {focussedMarker.location.street1 && <span className="address-section">{focussedMarker.location.street1}</span>}
                  {focussedMarker.location.suburb && <span className="address-section">{focussedMarker.location.suburb}</span>}
                  {focussedMarker.location.state && <span className="address-section">{focussedMarker.location.state}</span>}
                </div>
              </div>
              {focussedMarker.instagram &&
                <div className="instagram">
                  <a href={`https://www.instagram.com/${focussedMarker.instagram}`} target="_blank">
                    <InstagramIcon />@{focussedMarker.instagram}
                  </a>
                </div>
              }
              {focussedMarker.website &&
                <div className="website">
                  <a
                    href={focussedMarker.website}
                    target="_blank"
                  >
                    <GlobeIcon />{this.shortenedWebsite(focussedMarker.website)}
                  </a>
                </div>
              }
              {focussedMarker.hours &&
                <div className="hours">
                  <OkIcon />{focussedMarker.hours}
                </div>
              }
              {focussedMarker.articleLink && <div className="article-link"><a href={focussedMarker.articleLink}>SEE DETAILS</a></div>}
            </div>
          </div>
        }

        <MediaQuery minWidth={415}>
          <div className="city-fullsize-nav">
            <svg
              onClick={onClickGoForwardPlace}
              className="city-nav"
              viewBox="0 0 32 56"
            >
              <path fill="#B8B8B8" fill-rule="evenodd" d="M0 0l32 27.533v.934L0 56v-2.333L30.197 28 0 2.333z"/>
            </svg>
          </div>
        </MediaQuery>
      </div>
    );
  }
}

export default City;
