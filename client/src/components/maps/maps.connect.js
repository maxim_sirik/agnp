import { connect } from 'react-redux';
import { withRouter } from 'react-router'

import Maps from './maps';
import { maps } from './maps.selectors';
import {
  fetchMaps,
  fetchMap,
  fetchInstagrams,
  changeMapType,
  updateMarkers,
  onMarkerClick,
} from './maps.actions';

/*
This is a redux specific function.
What is does is: It gets the state specified in here from the global redux state.
For example, here we are retrieving the list of items from the redux store.
Whenever this list changes, any component that is using this list of item will re-render.
 */
const mapStateToProps = state => ({
  focussedMarker: state.rootReducer.mapsReducer.focussedMarker,
  markers: state.rootReducer.mapsReducer.markers,
  animate: state.rootReducer.mapsReducer.animate,
  resetView: state.rootReducer.mapsReducer.resetView,
  zoom: state.rootReducer.mapsReducer.zoom,
  mapType: state.rootReducer.mapsReducer.mapType,
  maps: state.rootReducer.mapsReducer.maps,
  instagrams: state.rootReducer.mapsReducer.instagrams,
});

/*
This is a redux specific function.
http://redux.js.org/docs/api/bindActionCreators.html
 */
const mapDispatchToProps = dispatch => ({
  getMaps: () => dispatch(fetchMaps()),
  getMap: slug => dispatch(fetchMap(slug)),
  getInstagrams: () => dispatch(fetchInstagrams()),
  onChangeMapType: type => dispatch(changeMapType(type)),
  onUpdateMarkers: () => dispatch(updateMarkers()),
  onMarkerClick: markerDetails => dispatch(onMarkerClick(markerDetails)),
});

/*
Here we are creating a Higher order component
https://facebook.github.io/react/docs/higher-order-components.html
 */
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Maps));
