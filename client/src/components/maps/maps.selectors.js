import { createSelector } from 'reselect';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';

export const maps = ({ mapsReducer }) => mapsReducer;

export const currentPlace = createSelector(maps, data => data.currentPlace);

export const mapMarkers = createSelector(maps, currentPlace, (selectMaps, selectCurrentPlace) => {
  if (isEmpty(selectCurrentPlace)) {
    return selectMaps.mapMarkers;
  }

  return selectCurrentPlace.locations;
});
