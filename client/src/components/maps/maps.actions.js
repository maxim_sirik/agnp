import { get } from 'axios';
import compact from 'lodash/compact';
import findIndex from 'lodash/findIndex';
import isEmpty from 'lodash/isEmpty';
import { createActions } from 'redux-actions';
import { push } from 'react-router-redux';

import { mapRoutes, MAP_TYPE, CITY_TYPE, INSTAGRAM_TYPE } from './maps.constants';
import { toggleSearch } from '../search/search.actions';

export const {
  setMapType,
  setMarkers,
  setFocussedMarker,
  fetchMapsRequest,
  fetchMapsSuccess,
  fetchMapsFailure,
} = createActions({
  SET_MAP_TYPE: mapType => ({ mapType }),
  SET_MARKERS: (markers, animate, resetView) => ({ markers, animate, resetView }),
  SET_FOCUSSED_MARKER: (focussedMarker, zoom, resetView) => ({ focussedMarker, zoom, resetView }),
  FETCH_MAPS_REQUEST: () => ({ error: false, fetching: true }),
  FETCH_MAPS_SUCCESS: response => ({
    error: false,
    fetching: false,
    response,
  }),
  FETCH_MAPS_FAILURE: error => ({ error, fetching: false })
});

export const fetchMaps = () => (dispatch, getState) => {
  dispatch(fetchMapsRequest());

  return get('/api/maps')
    .then(response => dispatch(fetchMapsSuccess(response)))
    .then(({ payload: { response: { data } } }) => dispatch(setMarkers(data, false)))
    .catch(error => dispatch(fetchMapsFailure(error)));
};

export const {
  fetchMapRequest,
  fetchMapSuccess,
  fetchMapFailure,
  updateMapMarkers,
} = createActions({
  FETCH_MAP_REQUEST: () => ({ error: false, fetching: true }),
  FETCH_MAP_SUCCESS: response => ({
    error: false,
    fetching: false,
    response,
  }),
  FETCH_MAP_FAILURE: error => ({ error, fetching: false }),
});

export const fetchMap = slug => (dispatch) => {
  dispatch(fetchMapRequest());

  return get(`/api/maps/${slug}`)
    .then(response => dispatch(fetchMapSuccess(response)))
    .catch((error) => {
      console.log('fetch map error:', error);
      dispatch(fetchMapFailure(error));
    });
};

export const {
  fetchInstagramsRequest,
  fetchInstagramsSuccess,
  fetchInstagramsFailure,
} = createActions({
  FETCH_INSTAGRAMS_REQUEST: () => ({ error: false, fetching: true }),
  FETCH_INSTAGRAMS_SUCCESS: response => ({
    error: false,
    fetching: false,
    response,
  }),
  FETCH_INSTAGRAMS_FAILURE: error => ({ error, fetching: false }),
});

export const fetchInstagrams = slug => (dispatch) => {
  dispatch(fetchInstagramsRequest());

  return get(`https://api.instagram.com/v1/users/self/media/recent/?access_token=${process.env.INSTAGRAM_TOKEN}`)
    .then((response) => {
      const instagrams = response.data.data.map(({
        id,
        images: { standard_resolution },
        caption,
        tags,
        location,
      }) => {
        if (!location) return false;

        return {
          id,
          image: standard_resolution,
          caption,
          tags,
          title: id,
          location: { geo: [location.longitude, location.latitude] },
        };
      });

      return dispatch(fetchInstagramsSuccess({ instagrams: compact(instagrams) }))
        .then(() => dispatch(updateMarkers()));
    })
    .catch((error) => {
      console.log('fetch instagrams error:', error);
      dispatch(fetchInstagramsFailure(error));
    });
};

export const updateMarkers = (resetView) => (dispatch, getState) => {
  const state = getState();
  const mapType = state.rootReducer.mapsReducer.mapType;
  const maps = state.rootReducer.mapsReducer.maps;
  const places = state.rootReducer.mapsReducer.currentPlace.places;

  if (mapType === MAP_TYPE) {
    return dispatch(setMarkers(maps, true));
  } else if (mapType === CITY_TYPE) {
    dispatch(setMarkers(places, true));
    return !isEmpty(places) && dispatch(setFocussedMarker(places[0], 14));
  } else if (mapType === INSTAGRAM_TYPE) {
    return dispatch(setMarkers(state.rootReducer.mapsReducer.instagrams, true, resetView));
  }

  return null;
};

export const changeMapType = (mapType, resetView) => (dispatch) => {
  dispatch(setMapType(mapType));
  dispatch(setFocussedMarker({}));
  dispatch(updateMarkers(resetView));
};

export const goToCity = (slug) => (dispatch, getState) => dispatch(push(`maps/${slug}`));

export const goToPlace = (placeId) => (dispatch, getState) => {
  const state = getState();
  const allPlaces = state.rootReducer.mapsReducer.currentPlace.places;
  const placeIndex = findIndex(allPlaces, ({ _id }) => _id === placeId);

  return dispatch(setFocussedMarker(state.rootReducer.mapsReducer.currentPlace.places[placeIndex], 14));
};

export const goBackPlace = () => (dispatch, getState) => {
  const state = getState();
  const allPlaces = state.rootReducer.mapsReducer.currentPlace.places;
  const focussedPlace = state.rootReducer.mapsReducer.focussedMarker;
  const focussedPlaceIndex = findIndex(allPlaces, ({ _id }) => _id === focussedPlace._id);
  const newFocussedPlaceIndex = (focussedPlaceIndex - 1) < 0 ? focussedPlaceIndex - 1 + allPlaces.length : focussedPlaceIndex - 1;

  return dispatch(setFocussedMarker(state.rootReducer.mapsReducer.currentPlace.places[newFocussedPlaceIndex], 14));
};

export const goForwardPlace = () => (dispatch, getState) => {
  const state = getState();
  const allPlaces = state.rootReducer.mapsReducer.currentPlace.places;
  const focussedPlace = state.rootReducer.mapsReducer.focussedMarker;
  const focussedPlaceIndex = findIndex(allPlaces, ({ _id }) => _id === focussedPlace._id);
  const newFocussedPlaceIndex = (focussedPlaceIndex + 1) > (allPlaces.length - 1) ? focussedPlaceIndex + 1 - allPlaces.length : focussedPlaceIndex + 1;

  return dispatch(setFocussedMarker(state.rootReducer.mapsReducer.currentPlace.places[newFocussedPlaceIndex], 14));
};