import { handleActions, combineActions } from 'redux-actions';
import {
  setMapType,
  setMarkers,
  setFocussedMarker,
  fetchMapsRequest,
  fetchMapsSuccess,
  fetchMapsFailure,
  fetchMapRequest,
  fetchMapSuccess,
  fetchMapFailure,
  fetchInstagramsRequest,
  fetchInstagramsSuccess,
  fetchInstagramsFailure,
} from './maps.actions';

const initialState = {
  mapType: null,
  markers: [],
  focussedMarker: {},
  animate: true,
  resetView: true,
  zoom: 3,
  maps: [],
  currentPlace: {},
  instagrams: [],
  fetching: null,
  error: null,
};

export default handleActions(
  {
    [setMapType]: (state, { payload: { mapType } }) => ({
      ...state,
      mapType,
    }),
    [setMarkers]: (state, { payload: { markers, animate, resetView } }) => ({
      ...state,
      markers,
      animate,
      resetView,
    }),
    [setFocussedMarker]: (state, { payload: { focussedMarker, zoom, resetView} }) => ({
      ...state,
      focussedMarker,
      zoom,
      resetView,
    }),
    [fetchMapsSuccess]: (state, { payload: { error, fetching, response } }) => ({
      ...state,
      error,
      fetching,
      maps: response.data,
    }),
    [fetchMapSuccess]: (state, { payload: { error, fetching, response } }) => ({
      ...state,
      error,
      fetching,
      currentPlace: response.data,
    }),
    [fetchInstagramsSuccess]: (state, { payload: { error, fetching, response: { instagrams } } }) => ({
      ...state,
      error,
      fetching,
      instagrams,
    }),
    [combineActions(
      fetchMapsRequest,
      fetchMapsFailure,
      fetchMapRequest,
      fetchMapFailure,
      fetchInstagramsRequest,
      fetchInstagramsFailure)](
      state,
      { payload: { error, fetching } }
    ) {
      return { ...state, error, fetching };
    },
  },
  initialState,
);
