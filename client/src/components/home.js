import React, { Component, PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ProgressiveImage from 'react-progressive-image';
import MediaQuery from 'react-responsive';
import classNames from 'classnames';
import { Parallax } from 'react-scroll-parallax';

import { fetchArticleImages } from './articles/articles.actions';

import Instagram from './instagram';
import Footer from './footer';
import isEmpty from "lodash/isEmpty";

class ParallaxElement extends Component {
  static contextTypes = {
    parallaxController: PropTypes.object.isRequired,
  };

  handleLoad = () => {
    // updates cached values after image dimensions have loaded
    this.context.parallaxController.update();
  };

  render() {
    return {...this.props.children};
  }
}

class Home extends PureComponent {
  constructor(props) {
    super(props);

    this.onScrollAnimations = this.onScrollAnimations.bind(this);

    this.galleryImages = [];
    this.parallaxElements = [];

    this.isMobile = (window.innerWidth || screen.width) < 1100;

    this.state = {
      galleryImages: [],
      fadeInImages: false,
      masonryOptions: {
        transitionDuration: 0,
      },
      imageLoadedCount: 0,
    }
  }

  componentWillMount() {
    const { getArticleImages } = this.props;
    getArticleImages();
  }

  componentDidMount() {
    window.setTimeout(() => window.addEventListener('scroll', this.onScrollAnimations), 500);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScrollAnimations);
  }

  componentDidUpdate() {
    const { articleImages } = this.props;

    if (articleImages.length > 0) {
      window.setTimeout(() => {
        this.parallaxElements.forEach((element) => {
          element.context.parallaxController.update();
        });

        this.homeGallery.classList.add('fadeInAndUp')
      }, 1000);
    }
  }

  onScrollAnimations() {
    this.galleryImages.forEach((image, index) => {
      const distanceFromTop = image.getBoundingClientRect().y + image.offsetHeight;
      const distanceFromBottom = window.innerHeight - image.getBoundingClientRect().y;

      if (distanceFromTop < 500) {
        image.style.opacity = (distanceFromTop / 5) * .01;
      } else if (distanceFromBottom > -10) {
        image.style.opacity = 1;
      } else if (distanceFromBottom < 0) {
        image.style.opacity = 0;
      }
    })

    if (this.hero.getBoundingClientRect().y < 70) {
      this.hero.classList.add('animate');
      this.nav.classList.add('animate');
    }

    const instaDistanceFromTop = this.instafeedContainer.getBoundingClientRect().y;

    if (instaDistanceFromTop < (window.innerHeight / 1.2)) {
      this.instafeedContainer.classList.add('animate');
    }
  }

  renderGalleryImages() {
    const { articleImages } = this.props;
    // const offSets = [
    //   '800px',
    //   '200px',
    //   '30px',
    //   '720px',
    //   '300px',
    //   '100px',
    //   '2400px',
    // ];

    // const aspectRatios = [
    //   'ar_0.65',
    //   'ar_0.70',
    //   'ar_1.3',
    //   'ar_0.75',
    //   'ar_1.3',
    //   'ar_0.70',
    //   'ar_0.95',
    // ];

    const aspectRatios = [
        '0.68',
        '1.46',
        '1.48',
        '0.68',
        '1.46',
        '1.48',
        '0.68'
    ];

    return articleImages.length > 0 && articleImages.slice(0, 7).map(((articleImage, index) => {
      const imageId = articleImage.heroImage.image.public_id;
      const articleSlug = articleImage.slug;

      return (
          <div className="article article-home">
              <Link to={`articles/${articleSlug}`} className="home-article-link" ref={(ref) => this.parallaxElements[index]}>
                  {/*<div className="article article-home">*/}
                      <img
                          ref={(ref) => this.galleryImages[index] = ref}
                          // src={`https://res.cloudinary.com/agnp/image/upload/${aspectRatios[index]},c_crop,q_auto/${imageId}`}
                          src={`https://res.cloudinary.com/agnp/image/upload/ar_${aspectRatios[index]},c_crop,dpr_auto,f_auto,q_auto/c_scale/${imageId}`}
                          onLoad={this.handleImagesLoaded}
                          style={{width: '100%'}}
                      />
                      <div className="content">
                          {articleImage.hasOwnProperty('categories') && articleImage.categories[0] &&
                          <div className="article-category">
                              <hr />
                              <span>{articleImage.categories[0].name}</span>
                          </div>
                          }
                          <h3 className="article-title">{articleImage.title}</h3>
                      </div>
                      <div className="overlay" />
                  {/*</div>*/}
              </Link>
          </div>
      );
    }));
  }

  render() {
    return (
      <div>
        <div className="home">
          {/*<div className="home-gallery" ref={(ref) => this.homeGallery = ref}>*/}
          <div className="articles" style={{opacity: 'unset'}}>
              <div className="articles-feed" ref={(ref) => this.homeGallery = ref}>
                  {this.renderGalleryImages()}
              </div>
          </div>
          <div className="hero" ref={(ref) => this.hero = ref}>
            <h1 id="title">Everyday Inspiration. Global Aspiration.</h1>
            <MediaQuery maxWidth={767}>
              <ProgressiveImage
                src="https://res.cloudinary.com/agnp/image/upload/ar_0.57,c_crop,dpr_auto,f_auto,q_auto:best/c_scale/agnp-home-hero2_hwct6a"
                placeholder="https://res.cloudinary.com/agnp/image/upload/ar_0.57,c_crop,dpr_auto,f_auto,q_1/c_scale/agnp-home-hero2_hwct6a"
              >
                {(src, loading) => (
                  <img
                    className="progressive-image"
                    className={classNames('progressive-image', { 'image-loaded': !loading })}
                    src={src}
                  />
                )}
              </ProgressiveImage>
            </MediaQuery>
            <MediaQuery minWidth={768} maxWidth={1100}>
              <ProgressiveImage
                src="https://res.cloudinary.com/agnp/image/upload/ar_0.74,c_crop,dpr_auto,f_auto,q_auto:best/c_scale/agnp-home-hero2_hwct6a"
                placeholder="https://res.cloudinary.com/agnp/image/upload/ar_0.74,c_crop,dpr_auto,f_auto,q_1/c_scale/agnp-home-hero2_hwct6a"
              >
                {(src, loading) => (
                  <img
                    className="progressive-image"
                    className={classNames('progressive-image', { 'image-loaded': !loading })}
                    src={src}
                  />
                )}
              </ProgressiveImage>
            </MediaQuery>
            <MediaQuery minWidth={1101}>
              <ProgressiveImage
                src="https://res.cloudinary.com/agnp/image/upload/ar_1.97,c_crop,dpr_auto,f_auto,q_auto:best/c_scale/agnp-home-hero2_hwct6a"
                placeholder="https://res.cloudinary.com/agnp/image/upload/ar_1.97,c_crop,dpr_auto,f_auto,q_1/c_scale/agnp-home-hero2_hwct6a"
              >
                {(src, loading) => (
                  <img
                    className="progressive-image"
                    className={classNames('progressive-image', { 'image-loaded': !loading })}
                    src={src}
                  />
                )}
              </ProgressiveImage>
            </MediaQuery>
            <div className="image-overlay" />
          </div>
          <div className="nav" ref={(ref) => this.nav = ref}>
            <div className="nav-item">
              <Link to="about" className="nav-link">ABOUT</Link>
            </div>
            <div className="nav-item">
              <Link to="maps" className="nav-link">MAP</Link>
            </div>
            <div className="nav-item">
              <Link to="articles" className="nav-link">ARTICLES</Link>
            </div>
            <div className="nav-item">
              <a href="https://www.studio.agnp.co/" className="nav-link">SHOP</a>
            </div>
          </div>
          <div className="instafeed-home-container" ref={(ref) => this.instafeedContainer = ref}>
            <Parallax
              disabled={this.isMobile}
              offsetYMin={-30}
            >
              <Instagram />
            </Parallax>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  articleImages: state.rootReducer.articlesReducer.articleImages,
});

const mapDispatchToProps = dispatch => ({
  getArticleImages: () => dispatch(fetchArticleImages()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
