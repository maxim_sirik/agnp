import React, { Component } from 'react';
import MediaQuery from 'react-responsive';
import { connect } from 'react-redux';
import classNames from 'classnames';
import CloseIcon from 'react-icons/lib/md/close';

import { submitEmail } from './footer/footer.actions';

const mapDispatchToProps = dispatch => ({
  onSubmitEmail: email => dispatch(submitEmail(email)),
});

const mapStateToProps = (state) => ({
  emailSubmitted: state.rootReducer.footerReducer.emailSubmitted,
});

class NewsletterModal extends Component {
  constructor(props) {
    super(props);

    this.onClickSubmitEmail = this.onClickSubmitEmail.bind(this);

    this.state = {
      duplicateError: false,
      slidDown: false,
      signUpSuccess: false,
    };
  }

  componentDidMount() {
    document.body.style.overflow = 'hidden'
    window.setTimeout(() => this.setState({ slideDown: true }), 3000);

  }

  componentWillUnmount() {
    document.body.style.overflow = 'scroll'
  }

  onClickSubmitEmail(ev) {
    ev.preventDefault()
    this.setState({ duplicateError: false });
    this.props.onSubmitEmail(this.emailInput.value)
      .then(({ payload }) => {
        if (payload.response.status === 200) {
          this.emailInput.value = "Thanks for signing up!";
          this.setState({ signUpSuccess: true });
        } else {
          this.setState({ duplicateError: true });
        }
      });
  }

  render() {
    const { onClickClose } = this.props;
    const { slideDown, signUpSuccess } = this.state;
    const width = window.innerWidth || screen.width;
    const height = window.innerHeight || screen.height;

    return (
      <div className={classNames('newsletter-modal', { 'slideDown': slideDown })}>
        <div className="modal-contents">
            <span
              className="newsletter-modal-close"
              onClick={onClickClose}>
              <CloseIcon />
            </span>
          <MediaQuery maxWidth={414}>
            <img src={`https://res.cloudinary.com/agnp/image/upload/w_${width},h_${height},c_crop,g_face,dpr_auto,f_auto,q_auto/c_scale/Newsletter-Overlay-Mobile_0006_background_buqsif`} />
          </MediaQuery>
          <MediaQuery minWidth={415}>
            <img src={'https://res.cloudinary.com/agnp/image/upload/c_crop,dpr_auto,f_auto,q_auto/c_scale/Newsletter-OverlayBG_odqplh'} />
          </MediaQuery>
          <div className="newsletter-form">
            <h2>The Weekly Newsletter</h2>
            <p>See what I’m currently into, every Friday in your inbox.</p>
            <form
              onSubmit={this.onClickSubmitEmail}
              className={classNames({ 'email-submitted': this.props.emailSubmitted })}
            >
              <input
                type="text"
                name="email"
                autocomplete="off"
                disabled={signUpSuccess}
                ref={(ref) => { this.emailInput = ref }}
              />

              <button type="submit">SUBSCRIBE</button>
            </form>
              <span className={classNames('duplicate-error', { 'active': this.state.duplicateError })}>
                It looks like you've already signed up, thanks!
              </span>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsletterModal);
