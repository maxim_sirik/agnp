import React, { Component } from 'react';
import Instafeed from 'react-instafeed';

import InstagramIcon from '../icons/instagram-maps';

const INSTAGRAM_TOKEN = process.env.INSTAGRAM_TOKEN;
const INSTAGRAM_CLIENT_ID = process.env.INSTAGRAM_CLIENT_ID;
const INSTAGRAM_USER_ID = process.env.INSTAGRAM_USER_ID;

class Instagram extends Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div className="instafeed-container">
        <a className="instafeed-title" href="https://www.instagram.com/aguynamedpatrick/">
          <InstagramIcon />@aguynamedpatrick
        </a>
        <div id="instafeed" className="instafeed">
          <Instafeed
            limit={6}
            ref="instafeed"
            resolution="standard_resolution"
            sortBy="most-recent"
            target="instafeed"
            userId="{INSTAGRAM_USER_ID}"
            clientId={INSTAGRAM_CLIENT_ID}
            accessToken={INSTAGRAM_TOKEN}
          />
        </div>
      </div>
    );
  }
}

export default Instagram;
