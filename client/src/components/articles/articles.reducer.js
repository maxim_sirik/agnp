import { handleActions, combineActions } from 'redux-actions';
import filter from 'lodash/filter';

import {
  fetchArticlesRequest,
  fetchArticlesSuccess,
  fetchArticlesFailure,
  fetchArticleImagesRequest,
  fetchArticleImagesSuccess,
  fetchArticleImagesFailure,
  fetchArticleRequest,
  fetchArticleSuccess,
  fetchArticleFailure,
  fetchFeaturedArticleRequest,
  fetchFeaturedArticleSuccess,
  fetchFeaturedArticleFailure,
  setFilter,
} from './articles.actions';

const initialState = {
  articleImages: [],
  allArticles: [],
  featuredArticle: null,
  currentPage: 1,
  totalPages: null,
  filter: 'all',
  currentArticle: null,
  fetching: null,
  error: null,
};

export default handleActions(
  {
    [fetchArticlesSuccess]: (state, { payload: { error, fetching, response } }) => ({
      ...state,
      error,
      fetching,
      allArticles: state.filter === response.data.filter ?
        [...state.allArticles, ...response.data.docs]
      :
        [...response.data.docs],
      currentPage: response.data.page,
      totalPages: response.data.pages,
      filter: response.data.filter,
    }),
    [fetchArticleImagesSuccess]: (state, { payload: { error, fetching, response } }) => ({
      ...state,
      error,
      fetching,
      articleImages: response.data,
    }),
    [fetchArticleSuccess]: (state, { payload: { error, fetching, response } }) => ({
      ...state,
      error,
      fetching,
      currentArticle: response.data,
    }),
    [fetchFeaturedArticleSuccess]: (state, { payload: { error, fetching, response } }) => ({
      ...state,
      error,
      fetching,
      featuredArticle: response.data,
    }),
    [setFilter]: (state, { payload: { filter } }) => ({
      ...state,
      filter,
    }),
    [combineActions(
      fetchArticlesRequest,
      fetchArticlesFailure,
      fetchArticleImagesRequest,
      fetchArticleImagesFailure,
      fetchArticleRequest,
      fetchArticleFailure,
      fetchFeaturedArticleRequest,
      fetchFeaturedArticleFailure)](
      state,
      { payload: { error, fetching } },
    ) {
      return { ...state, error, fetching };
    },
  },
  initialState,
);
