import React, { Component } from 'react';
import each from 'lodash/each';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import renderHTML from 'react-render-html';
import isEmpty from 'lodash/isEmpty';
import MediaQuery from 'react-responsive';
import ProgressiveImage from 'react-progressive-image';
import { Image, Transformation } from 'cloudinary-react';
import classNames from 'classnames';
import DownArrowIcon from 'react-icons/lib/fa/angle-down';
import { Parallax } from 'react-scroll-parallax';

import Instagram from '../instagram';
import Footer from '../footer';

const articleCategories = {
  story: "Stories",
  place: "Places"
}

const CATEGORIES = [
  'New York City',
  'Los Angeles',
  'Brunch',
  'Hidden Spots',
  'Cocktails'
]

class Articles extends Component {
  constructor(props) {
    super(props);

    this.onScrollAnimations = this.onScrollAnimations.bind(this);

    this.isMobile = (window.innerWidth || screen.width) < 1100;

    this.articleRefs = {};

    this.state = {
      loadAnimate: false
    }
  }

  componentWillMount() {
    const { articles, getArticles, getFeaturedArticle, setFilter } = this.props;

    if (isEmpty(articles)) {
      setFilter('all'); // Also fetches all the articles
      getFeaturedArticle();
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScrollAnimations);
    window.setTimeout(() => this.setState({ loadAnimate: true }), 1500);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScrollAnimations);
  }

  onScrollAnimations() {
    !isEmpty(this.articleRefs) && each(this.articleRefs, (article, index) => {
      if (!article) return;
      const distanceFromTop = article.getBoundingClientRect().y + article.offsetHeight;
      const distanceFromBottom = window.innerHeight - article.getBoundingClientRect().y;

      if (distanceFromBottom > -10) {
        article.style.top = 0;
      } else if (distanceFromBottom < 0) {
        article.style.top = '200px';
      }
    })
  }

  renderFeaturedArticle() {
    const { featuredArticle: { title, slug, categories, heroImage: { image } } } = this.props;

    return (
      <div className="featured-article">
        <Link to={`/articles/${slug}`}>
          <div className="featured-image">
            <MediaQuery maxWidth={767}>
              <ProgressiveImage
                src={`https://res.cloudinary.com/agnp/image/upload/ar_0.75,c_crop,dpr_auto,f_auto,q_auto/c_scale/${image.public_id}`}
                placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_0.75,c_crop,dpr_auto,f_auto,q_1/c_scale/${image.public_id}`}
              >
                {(src, loading) => (
                  <img className="progressive-image" style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
                )}
              </ProgressiveImage>
            </MediaQuery>
            <MediaQuery minWidth={768} maxWidth={1100}>
              <ProgressiveImage
                src={`https://res.cloudinary.com/agnp/image/upload/ar_1.41,c_crop,dpr_auto,f_auto,q_auto/c_scale/${image.public_id}`}
                placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_1.41,c_crop,dpr_auto,f_auto,q_1/c_scale/${image.public_id}`}
              >
                {(src, loading) => (
                  <img className="progressive-image" style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
                )}
              </ProgressiveImage>
            </MediaQuery>
            <MediaQuery minWidth={1101} maxWidth={2560}>
              <ProgressiveImage
                src={`https://res.cloudinary.com/agnp/image/upload/ar_2.2,c_crop,dpr_auto,f_auto,q_auto/c_scale/${image.public_id}`}
                placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_2.2,c_crop,dpr_auto,f_auto,q_1/c_scale/${image.public_id}`}
              >
                {(src, loading) => (
                  <img className="progressive-image" style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
                )}
              </ProgressiveImage>
            </MediaQuery>
            <MediaQuery minWidth={2561}>
              <ProgressiveImage
                src={`https://res.cloudinary.com/agnp/image/upload/ar_1.9,c_crop,dpr_auto,f_auto,q_auto/c_scale/${image.public_id}`}
                placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_1.9,c_crop,dpr_auto,f_auto,q_1/c_scale/${image.public_id}`}
              >
                {(src, loading) => (
                  <img className="progressive-image" style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
                )}
              </ProgressiveImage>
            </MediaQuery>
            <div className="image-overlay" />
          </div>
        </Link>

        <div className="featured-title">
          <Link to={`/articles/${slug}`}>
            <div className="article-category">
              <hr />
              <span>{categories[0].name}</span>
            </div>
            <h2>{title}</h2>
            <Link className="see-more" to={`/articles/${slug}`}>READ MORE</Link>
          </Link>
        </div>
      </div>
    );
  }

  renderArticles(articles) {
    const aspectRatios = [
      '0.68',
      '1.46',
      '1.48'
    ];

    return articles && articles.map((article, index) => (
      <div
        className="article"
        key={article.slug}
        ref={(ref) => this.articleRefs[article.slug] = ref}
      >
        {article.heroImage.image &&
          <Link to={`/articles/${article.slug}`}>
            <div className="article-image">
              <img
                src={`https://res.cloudinary.com/agnp/image/upload/ar_${aspectRatios[index]},c_crop,dpr_auto,f_auto,q_auto/c_scale/${article.heroImage.image.public_id}`}
              />
            </div>
          </Link>
        }
        {article.categories[0] &&
          <div className="article-category">
            <hr />
            <span>{article.categories[0].name}</span>
          </div>
        }
        <h3 className="article-title">{article.title}</h3>
        {article.brief &&
          <p className="article-brief">{article.brief}</p>
        }
        <Link className="see-more" to={`/articles/${article.slug}`}>READ MORE</Link>
      </div>
    ));
  }

  renderEmptyState() {
    return (
      <div className="no-articles">
        <h2>Sorry no articles</h2>
      </div>
    );
  }

  render() {
    const {
      onToggleSearch,
      featuredArticle,
      articles,
      getArticles,
      setFilter,
      currentPage,
      totalPages,
      filter,
      categories,
    } = this.props;

    const firstArticles = articles.slice(0, 3);
    const restOfArticles = articles.slice(3);;
    var articleSets = restOfArticles.map((e, i) => i % 3 === 0 ? restOfArticles.slice(i, i + 3) : null)
      .filter((e) => e);

    return (
      <div>
        <div className={classNames('articles', { animate: this.state.loadAnimate })}>
          {featuredArticle && this.renderFeaturedArticle()}

          <div className="category-filter">
            <div className="category-filter-title">
              <hr />
              <span>View By Category</span>
              <hr />
            </div>
            <div className="category-list">
              <span
                className="category"
                className={classNames('category', { active: filter === 'all' })}
                onClick={() => setFilter('all')}
              >
                ALL
              </span>
              {categories && categories.map((category) => (
                <span
                  className="category"
                  className={classNames('category', { active: filter === category.key })}
                  onClick={() => setFilter(category.key)}
                >
                  {category.name}
                </span>
              ))}
            </div>
          </div>

          <div className="articles-feed">
            {firstArticles.length > 0 ?
              this.renderArticles(firstArticles) :
              this.renderEmptyState()
            }
          </div>

          <div className="search-box" style={{ display: 'none' }}>
            <h3>What would you like to discover?</h3>
            <hr />
            <div className="tags">
              <span className="tags-list">
                {CATEGORIES.map((category) =>
                  <span className="tag">
                    {category}
                  </span>
                )}
              </span>
              <div className="tags-search">
                <span className="or-try">or try</span>
                <button
                  className="search-box-button"
                  onClick={() => onToggleSearch(true)}
                >
                  Search
                </button>
              </div>
            </div>
          </div>

          {articleSets.map(articleSet => (
            <div className="articles-feed">
              {this.renderArticles(articleSet)}
            </div>
          ))}

          {currentPage < totalPages &&
            <span className="load-more" onClick={() => getArticles({ pageNum: currentPage + 1 })}>
              LOAD MORE ARTICLES
              <DownArrowIcon />
            </span>
          }
        </div>
        <Instagram />
        <Footer />
      </div>
    );
  }
}

export default Articles;
