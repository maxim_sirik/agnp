import React from 'react';
import renderHTML from 'react-render-html';
import { Image, Transformation } from 'cloudinary-react';

export default ({
  author,
  categories,
  title,
  subheader,
  body,
  heroImage,
}) => (
  <div className="article-container">
    <h1>{title}</h1>
    <h3>{author.name.first} {author.name.last}</h3>
    <h3>{subheader}</h3>
    <div className="hero-image">
      <Image publicId={heroImage.image.public_id} crop="scale">
        <Transformation quality="auto" fetch_format="auto" />
        <Transformation width="auto" dpr="auto" crop="fit" />
      </Image>
    </div>
    <p className="body">{renderHTML(body)}</p>
  </div>
);
