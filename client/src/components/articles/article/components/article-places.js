import React, { Component } from 'react';
import { Image, Transformation } from 'cloudinary-react';
import MapMarkerIcon from 'react-icons/lib/fa/map-marker';
import ArrowIcon from 'react-icons/lib/fa/long-arrow-right';
import InstagramIcon from '../../../../icons/instagram';
import GlobeIcon from '../../../../icons/globe';

import MapContainer from '../../../map-container';
import { ARTICLE_LIST_TYPE } from '../../../maps/maps.constants';

const shortenedWebsite = (website) => {
  if (!website) return;
  const linkElement = document.createElement("a");
  linkElement.href = website;
  return linkElement.hostname;
};

const renderPlace = ({
  articleLink,
  number,
  title,
  slug,
  image,
  brief,
  location,
  instagram,
  website,
}) => {
  const formattedNumber = number < 10 ? number.toString().match(/[0-9]/)[0].padStart(2, '0') : number;

  return (
    <div className="place">
      <div className="place-image">
        <Image publicId={image.public_id} crop="scale">
          <Transformation quality="auto" fetch_format="auto" aspect_ratio="1" crop="crop" dpr="auto" />
        </Image>
      </div>
      <div className="place-description">
        <h3 className="number">{formattedNumber}</h3>
        <h3 className="place-title">{title}</h3>
        <p className="brief">{brief}</p>
        <div className="address">
          <MapMarkerIcon />
          {`${location.street1}, ${location.suburb}, ${location.state}`}
        </div>
        <div className="instagram">
          <InstagramIcon />
          <a
            href={`https://instagram.com/${instagram}`}
            target="_blank"
          >
            {instagram}
          </a>
        </div>
        <div className="website"><GlobeIcon />
          <a
            href={website}
            target="_blank"
          >
            {shortenedWebsite(website)}
          </a>
        </div>
        {articleLink && <a href={articleLink} className="link">See more details <ArrowIcon /></a>}
      </div>
    </div>
  );
};

export default ({ placesTitle, places }) => (
  <div className="article-places">
    <h1 className="title">{placesTitle}</h1>
    <MapContainer
      markers={places.map((place, index) => ({
        ...place,
        number: index + 1,
      }))}
      context='article'
      height={500}
      mapType={ARTICLE_LIST_TYPE}
      settings={{
        dragPan: true,
        dragRotate: true,
        scrollZoom: false,
        touchZoom: false,
        touchRotate: true,
        doubleClickZoom: false,
      }}
    />
    {places.map((place, index) => {
      const number = index + 1;
      return renderPlace({ ...place, number });
    })}
  </div>
);
