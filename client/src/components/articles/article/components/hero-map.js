import React from 'react';
import MediaQuery from 'react-responsive';
import MapContainer from '../../../map-container';
import { Image, Transformation } from 'cloudinary-react';
import ProgressiveImage from 'react-progressive-image';

import MapMarkerIcon from 'react-icons/lib/fa/map-marker';
import PhoneIcon from '../../../../icons/phone';
import GlobeIcon from '../../../../icons/globe';
import ClockIcon from '../../../../icons/clock';
import MapIcon from '../../../../icons/map';
import InstagramIcon from '../../../../icons/instagram';
import { ARTICLE_HERO_TYPE } from '../../../maps/maps.constants';

const mapWidth = () => {
  const pageWidth = window.innerWidth || screen.width;

  if (pageWidth < 768) {
    return pageWidth;
  } else if (pageWidth < 1100) {
    return pageWidth * 0.50;
  } else {
    return pageWidth * 0.38;
  }
};

const mapHeight = () => {
  const pageWidth = window.innerWidth || screen.width;

  if (pageWidth < 768) {
    return 195;
  } else if (pageWidth < 1100) {
    return mapWidth() * 0.87;
  } else {
    return 373;
    //return mapWidth() * 0.42;
  }
};

const shortenedWebsite = (website) => {
  if (!website) return;
  const linkElement = document.createElement("a");
  linkElement.href = website;
  return linkElement.hostname;
};

export default ({ image: { image }, place }) => (
  <div className="hero-map">
    <div className="hero-map-image">
      <MediaQuery maxWidth={767}>
        <ProgressiveImage
          src={`https://res.cloudinary.com/agnp/image/upload/ar_1.25,c_crop,dpr_auto,f_auto,q_auto:best/c_scale/${image.public_id}`}
          placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_1.25,c_crop,dpr_auto,f_auto,q_1/c_scale/${image.public_id}`}
        >
          {(src, loading) => (
            <img className="progressive-image" style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
          )}
        </ProgressiveImage>
      </MediaQuery>
      <MediaQuery minWidth={768}>
        <ProgressiveImage
          src={`https://res.cloudinary.com/agnp/image/upload/ar_1.75,c_crop,dpr_auto,f_auto,q_auto:best/c_scale/${image.public_id}`}
          placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_1.75,c_crop,dpr_auto,f_auto,q_1/c_scale/${image.public_id}`}
        >
          {(src, loading) => (
            <img className="progressive-image" style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
          )}
        </ProgressiveImage>
      </MediaQuery>
    </div>
    <div className="hero-map-lower">
      <div className="hero-map-map">
        <MapContainer
          markers={[place]}
          context='article'
          height={mapHeight()}
          width={mapWidth()}
          mapType={ARTICLE_HERO_TYPE}
          zoom={13}
          settings={{
            dragPan: false,
            dragRotate: false,
            scrollZoom: false,
            touchZoom: false,
            touchRotate: false,
            doubleClickZoom: false,
          }}
        />
      </div>
      <div className="hero-map-info">
        <div className="hero-map-info-item address">
          <div className="info-title">
            <MapMarkerIcon />
            <h3>Address</h3>
          </div>
          <span>{`${place.location.street1}, ${place.location.suburb}, ${place.location.state}`}</span>
        </div>
        {place.phone &&
          <div className="hero-map-info-item phone">
            <div className="info-title">
              <PhoneIcon />
              <h3>Phone</h3>
            </div>
            <span>{place.phone}</span>
          </div>
        }
        {place.website &&
          <div className="hero-map-info-item website">
            <div className="info-title">
              <GlobeIcon />
              <h3>Website</h3>
            </div>
            <span><a href={place.website} target="_blank">{shortenedWebsite(place.website)}</a></span>
          </div>
        }
        {place.hours &&
          <div className="hero-map-info-item hours">
            <div className="info-title">
              <ClockIcon />
              <h3>Go For</h3>
            </div>
            <span>{place.hours}</span>
          </div>
        }
        {place.neighborhood &&
          <div className="hero-map-info-item neighborhood">
            <div className="info-title">
              <MapIcon />
              <h3>Neighborhood</h3>
            </div>
            <span>{place.neighborhood}</span>
          </div>
        }
        {place.instagram &&
          <div className="hero-map-info-item instagram">
            <div className="info-title">
              <InstagramIcon />
              <h3>Instagram</h3>
            </div>
            <span>
              <a href={`https://www.instagram.com/${place.instagram}/`} target="_blank">@{place.instagram}</a>
            </span>
          </div>
        }
      </div>
    </div>
    <div className="hero-map-background" />
  </div>
);
