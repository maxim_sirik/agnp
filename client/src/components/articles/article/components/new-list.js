import React from 'react';
import renderHTML from 'react-render-html';
import each from 'lodash/each';
import { Image, Transformation } from 'cloudinary-react';

const checkStacked = (type) => (type.indexOf('stacked') !== -1);

const hasNextList = (lists, currIdx) => {
  let i = currIdx + 1;
  while (i < 5) {
    if (lists[i].items.length > 0) return true;

    ++i;
  }

  return false;
};

const renderItems = (items, type, isLast = false) => {
  const itemComponents = [];
  const isStacked = checkStacked(type);

  each(items, (item, key) => item.heading && item.content && itemComponents.push(
    <div className={type}>
      {(item.image) && (
        isStacked ? (
          <a href={item.link || '#'}>
            <Image publicId={item.image.public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" aspect_ratio="1" crop="crop" dpr="auto" />
            </Image>
          </a>
        ) : (
          <Image publicId={item.image.public_id} crop="scale">
            <Transformation quality="auto" fetch_format="auto" aspect_ratio="1" crop="crop" dpr="auto" />
          </Image>
        )
      )}
      <div className="new-item-text">
        {(type.indexOf('not-numbered') === -1) && (
          <h3 className="item-number">{(key < 10 ? '0' : '') + (key + 1)}</h3>
        )}
        {isStacked ? (
          <a href={item.link || '#'}>
            <h3 className="item-heading">{item.heading}</h3>
          </a>
        ) : (
          <h3 className="item-heading">{item.heading}</h3>
        )}
        <div className="new-item-content">
          {renderHTML(item.content)}
        </div>
        {(isStacked && !(key === items.length - 1 && isLast)) ? <p>&nbsp;</p> : ''}
      </div>
    </div>,
  ));

  return itemComponents;
};

export default ({ type, direction, lists }) => (
  <div className={`${(type.indexOf('alternating') !== -1) ? `${direction || 'left-right'} article-new-list` : 'body-text article-new-list'}`}>
    {(checkStacked(type) && lists[0].title) && <h2>{lists[0].title}</h2>}
    {(lists[0].items.length > 0) && renderItems(lists[0].items, type, !hasNextList(lists, 0))}
    {(checkStacked(type) && lists[1].title) && <h2>{lists[1].title}</h2>}
    {(lists[1].items.length > 0) && renderItems(lists[1].items, type, !hasNextList(lists, 1))}
    {(checkStacked(type) && lists[2].title) && <h2>{lists[2].title}</h2>}
    {(lists[2].items.length > 0) && renderItems(lists[2].items, type, !hasNextList(lists, 2))}
    {(checkStacked(type) && lists[3].title) && <h2>{lists[3].title}</h2>}
    {(lists[3].items.length > 0) && renderItems(lists[3].items, type, !hasNextList(lists, 3))}
    {(checkStacked(type) && lists[4].title) && <h2>{lists[4].title}</h2>}
    {(lists[4].items.length > 0) && renderItems(lists[4].items, type, true)}
  </div>
);
