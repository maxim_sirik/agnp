import React from 'react';
import isEmpty from 'lodash/isEmpty';
import MediaQuery from 'react-responsive';
import { Image, Transformation } from 'cloudinary-react';

export default ({ align, images, fullWidth }) => {
  if (images.length === 3) {
    if (fullWidth) {
      return (
        <div className="gallery">
          <div className="gallery-full-width">
            <Image publicId={images[0].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" aspect_ratio="1" crop="crop" dpr="auto" />
            </Image>
            <Image publicId={images[1].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" width="auto" aspect_ratio="1" crop="crop" dpr="auto" />
            </Image>
            <Image publicId={images[2].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" aspect_ratio="1" crop="crop" dpr="auto" />
            </Image>
          </div>
        </div>
      );
    } else if (align === 'left') {
      return (
        <div className="gallery">
          <div className="gallery-column gallery-column-square">
            <Image publicId={images[0].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" aspect_ratio="1" crop="crop" dpr="auto" />
            </Image>
            <Image publicId={images[2].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" aspect_ratio="1" crop="crop" dpr="auto" />
            </Image>
          </div>
          <div className="gallery-column gallery-column-vertical">
            <Image publicId={images[1].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" width="auto" aspect_ratio="0.75" crop="crop" dpr="auto" />
            </Image>
          </div>
        </div>
      );
    } else if (align === 'right') {
      return (
        <div className="gallery">
          <div className="gallery-column gallery-column-vertical">
            <Image publicId={images[0].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" width="auto" aspect_ratio="0.75" crop="crop" dpr="auto" />
            </Image>
          </div>
          <div className="gallery-column gallery-column-square">
            <Image publicId={images[1].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" aspect_ratio="1" crop="crop" dpr="auto" />
            </Image>
            <Image publicId={images[2].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" aspect_ratio="1" crop="crop" dpr="auto" />
            </Image>
          </div>
        </div>
      );
    }
  } else if (images.length === 2) {
      return (
        <div className="gallery">
          <div className="gallery-column gallery-column-vertical">
            <Image publicId={images[0].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" width="auto" aspect_ratio="0.75" crop="crop" dpr="auto" />
            </Image>
          </div>
          <div className="gallery-column gallery-column-vertical">
            <Image publicId={images[1].public_id} crop="scale">
              <Transformation quality="auto" fetch_format="auto" width="auto" aspect_ratio="0.75" crop="crop" dpr="auto" />
            </Image>
          </div>
        </div>
      );
    } else {
    return false;
  }
};
