import React from 'react';

export default ({ categories }) => (
  <div className="article-categories">
    <hr className="categories-rule" />
    <ul>
      {categories.map((category) => {
        return (<li className="article-category">{category.name}</li>);
      })}
    </ul>
  </div>
);
