import React from 'react';
import chain from 'lodash/chain';
import each from 'lodash/each';
import sortBy from 'lodash/sortBy';
import map from 'lodash/map';

const itemNumber = (key) => key.match(/\d+/)[0].padStart(2, '0');

const renderItems = (items, type) => {
  const itemComponents = [];
  const mappedItems = map(items, (value, key) => ({ ...value, key }));

  const sortedItems = sortBy(mappedItems, (item) => {
    return parseInt(item.key.match(/\d+/)[0]);
  });

  each(sortedItems, (item) => item.heading && item.content && itemComponents.push(
    <div className={`list-item ${type}`}>
      <h3 className="item-number">{itemNumber(item.key)}</h3>
      <h3 className="item-heading">{item.heading}</h3>
      <hr />
      <p className="item-content">{item.content}</p>
    </div>,
  ));

  return itemComponents;
};

export default ({ title, type, items }) => (
  <div className="article-list">
    <h2 className="title">{title}</h2>
    <div className="article-list-items">
      {renderItems(items, type)}
    </div>
  </div>
);
