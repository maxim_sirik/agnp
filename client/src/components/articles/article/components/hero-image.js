import React from 'react';
import MediaQuery from 'react-responsive';
import ProgressiveImage from 'react-progressive-image';

export default ({ image, caption, link }) => (
  <div className="hero-image">
    <MediaQuery maxWidth={767}>
      <ProgressiveImage
        src={`https://res.cloudinary.com/agnp/image/upload/ar_1.25,c_crop,dpr_auto,f_auto,q_auto:best/c_scale/${image.public_id}`}
        placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_1.25,c_crop,dpr_auto,f_auto,q_1/c_scale/${image.public_id}`}
      >
        {(src, loading) => (
          <img className="progressive-image" style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
        )}
      </ProgressiveImage>
    </MediaQuery>
    <MediaQuery minWidth={768}>
      <ProgressiveImage
        src={`https://res.cloudinary.com/agnp/image/upload/ar_1.75,c_crop,dpr_auto,f_auto,q_auto:best/c_scale/${image.public_id}`}
        placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_1.75,c_crop,dpr_auto,f_auto,q_1/c_scale/${image.public_id}`}
      >
        {(src, loading) => (
          <img className="progressive-image" style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
        )}
      </ProgressiveImage>
    </MediaQuery>
    {caption &&
      <div className="image-caption">
        <h3>{caption}</h3>
        {link &&
          <a href={link} target="_blank">See on Instagram</a>
        }
      </div>
    }
  </div>
);
