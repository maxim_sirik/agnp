import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import renderHTML from 'react-render-html';
import classNames from 'classnames';
import MediaQuery from 'react-responsive';
import ProgressiveImage from 'react-progressive-image';
import { JSONLD, Generic } from 'react-structured-data';

import HeroImage from './components/hero-image';
import HeroMap from './components/hero-map';
import Categories from './components/categories';
import Gallery from './components/gallery';
import ArticleList from './components/article-list';
import NewList from './components/new-list';
import ArticlePlaces from './components/article-places';
import Instagram from '../../instagram';
import Footer from '../../footer';

import ArticleNewsletter from './components/article-newsletter';

class Article extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loadAnimate: false
    }
  }

  componentWillMount() {
    this.props.getArticle();
  }

  componentDidMount() {
    window.setTimeout(() => this.setState({ loadAnimate: true }), 500);
  }

  componentDidUpdate() {
    if (this.props.article) document.title = `${this.props.article.title} - A Guy Named Patrick`;

    return this.props.article &&
      this.props.article.slug !== this.props.match.params.slug &&
      this.props.getArticle();
  }

  renderStructuredContent() {
    const { article: { author, heroImage, title, publishedDate, slug } } = this.props;

    const imageId = heroImage.image && heroImage.image.public_id;

    return (
      <JSONLD>
        <Generic
          type="article"
          jsonldtype="Article"
          schema={{
            author: {
              '@type': 'Person',
              name: `${author.name.first} ${author.name.last}`,
            },
            description: title,
            image: `https://res.cloudinary.com/agnp/image/upload/ar_1.77,c_crop,dpr_auto,f_auto,q_auto:best/c_scale/${imageId}`,
            datePublished: publishedDate,
            headline: title,
            publisher: {
              '@type': 'Organization',
              name: 'A Guy Named Patrick',
              logo: {
                '@type': 'ImageObject',
                url: 'https://agnp.co/favicon.ico'
              },
            },
            mainEntityOfPage: {
              '@type': 'WebPage',
              '@id': `https://agnp.co/articles/${slug}`,
            },
          }}
        />
      </JSONLD>
    );
  }

  renderHero() {
    const { article: { postType, heroImage, place } } = this.props;

    if (heroImage.image && !isEmpty(place) && postType === 'shortform') {
      return (
        <HeroMap image={heroImage} place={place} />
      );
    } else if (heroImage.image) {
      return (
        <HeroImage {...heroImage} />
      );
    } else {
      return false;
    }
  }

  render() {
    if (!this.props.article) return null;

    const {
      article: {
        postType,
        title,
        author,
        subheader,
        categories,
        bodyContent,
        gallery1,
        gallery2,
        gallery3,
        newGallery1,
        gallery1fullWidth,
        newGallery2,
        gallery2fullWidth,
        newGallery3,
        gallery3fullWidth,
        articleList,
        placesTitle,
        places,
        imageLink,
        // new list
        type2,
        listDirection2,
        list1Title,
        list2Title,
        list3Title,
        list4Title,
        list5Title,
        listItems1,
        listItems2,
        listItems3,
        listItems4,
        listItems5,
      },
    } = this.props;

    const articleLists = [
      {
        title: list1Title,
        items: listItems1 || [],
      },
      {
        title: list2Title,
        items: listItems2 || [],
      },
      {
        title: list3Title,
        items: listItems3 || [],
      },
      {
        title: list4Title,
        items: listItems4 || [],
      },
      {
        title: list5Title,
        items: listItems5 || [],
      },
    ];

    return (
      <div>
        {this.renderStructuredContent()}
        <div className={classNames('article-container', { animate: this.state.loadAnimate })}>
          <h1 className="article-title">
            {title}
          </h1>
          <hr className="title-rule" />
          {subheader && <h2 className="article-subheader">{subheader}</h2>}

          {this.renderHero()}

          <div className="byline">
            {!isEmpty(categories) && <Categories categories={categories} />}

            <div className="author">
              <p className="author-written">written by</p>
              <p className="author-name">{author.name.first} {author.name.last}</p>
            </div>
          </div>

          {!isEmpty(bodyContent.body1) &&
            <React.Fragment>
              <div className="body-text body-one">
                {renderHTML(bodyContent.body1)}
              </div>

              <ArticleNewsletter />
              {/* TODO append newsletter form !!! */}
            </React.Fragment>
          }

          <div className="galleries">
            {!isEmpty(gallery1) && <Gallery align="left" images={gallery1} />}
            {!isEmpty(gallery2) && <Gallery align="right" images={gallery2} />}
            {!isEmpty(gallery3) && <Gallery align="right" images={gallery3} />}
          </div>

          {(newGallery1 && !isEmpty(newGallery1.images)) && (
            <div className="galleries">
              <Gallery align="left" images={newGallery1.images} fullWidth={gallery1fullWidth} />
            </div>
          )}

          {!isEmpty(bodyContent.body2) &&
            <div className="body-text body-two">
              {renderHTML(bodyContent.body2)}
            </div>
          }
          {isEmpty(bodyContent.body1) &&
            <React.Fragment>
              <ArticleNewsletter />
            {/* TODO append newsletter form !!! */}
            </React.Fragment>
          }

          {/* OLD list */}
          {!isEmpty(articleList.items.item1.content) && <ArticleList {...articleList} />}
          {postType === 'longform' && !isEmpty(places) && <ArticlePlaces placesTitle={placesTitle} places={places} />}
          {/* END OLD list */}

          {/* NEW list (image based) */}
          {(!isEmpty(listItems1) || !isEmpty(listItems2) || !isEmpty(listItems3) || !isEmpty(listItems4) || !isEmpty(listItems5)) && <NewList type={type2} direction={listDirection2} lists={articleLists} />}
          {/* END NEW list (image based) */}


          {!isEmpty(bodyContent.body3) &&
            <div className="body-text body-two">
              {renderHTML(bodyContent.body3)}
            </div>
          }

          {(newGallery2 && !isEmpty(newGallery2.images)) && (
            <div className="galleries">
              <Gallery align="right" images={newGallery2.images} fullWidth={gallery2fullWidth} />
            </div>
          )}


          {!isEmpty(bodyContent.body4) &&
            <div className="body-text body-two">
              {renderHTML(bodyContent.body4)}
            </div>
          }

          {(newGallery3 && !isEmpty(newGallery3.images)) && (
            <div className="galleries">
              <Gallery align="left" images={newGallery3.images} fullWidth={gallery3fullWidth} />
            </div>
          )}

          {!isEmpty(bodyContent.body5) &&
            <div className="body-text body-two">
              {renderHTML(bodyContent.body5)}
            </div>
          }

          {!isEmpty(imageLink && imageLink.image) &&
            <div className="image-link">
              <MediaQuery maxWidth={767}>
                <ProgressiveImage
                  src={`https://res.cloudinary.com/agnp/image/upload/ar_0.86,c_crop,dpr_auto,f_auto,q_auto:best/c_scale/${imageLink.image.public_id}`}
                  placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_0.86,c_crop,dpr_auto,f_auto,q_1/c_scale/${imageLink.image.public_id}`}
                >
                  {(src, loading) => (
                    <img className="progressive-image" style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
                  )}
                </ProgressiveImage>
              </MediaQuery>
              <MediaQuery minWidth={768}>
                <ProgressiveImage
                  src={`https://res.cloudinary.com/agnp/image/upload/ar_1.35,c_crop,dpr_auto,f_auto,q_auto:best/c_scale/${imageLink.image.public_id}`}
                  placeholder={`https://res.cloudinary.com/agnp/image/upload/ar_1.35,c_crop,dpr_auto,f_auto,q_1/c_scale/${imageLink.image.public_id}`}
                >
                  {(src, loading) => (
                    <img className="progressive-image" style={{ opacity: loading ? 0.5 : 1 }} src={src}/>
                  )}
                </ProgressiveImage>
              </MediaQuery>
              <div className="image-overlay" />
              <div className="image-link-content">
                  <h3>{imageLink.title}</h3>
                  <a href={imageLink.url}>Click Here</a>
              </div>
            </div>
          }

          <Instagram />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Article;
