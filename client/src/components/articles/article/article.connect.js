import { connect } from 'react-redux';
import get from 'lodash/get';
import Article from './article';

import { fetchArticle } from '../articles.actions';

/*
This is a redux specific function.
What is does is: It gets the state specified in here from the global redux state.
For example, here we are retrieving the list of items from the redux store.
Whenever this list changes, any component that is using this list of item will re-render.
 */
const mapStateToProps = state => ({
  article: state.rootReducer.articlesReducer.currentArticle,
});

/*
This is a redux specific function.
http://redux.js.org/docs/api/bindActionCreators.html
 */
const mapDispatchToProps = (dispatch, ownProps) => ({
  getArticle: () => dispatch(fetchArticle(ownProps.match.params.slug)),
});



/*
Here we are creating a Higher order component
https://facebook.github.io/react/docs/higher-order-components.html
 */
export default connect(mapStateToProps, mapDispatchToProps)(Article);
