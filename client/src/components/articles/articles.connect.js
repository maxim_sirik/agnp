import { connect } from 'react-redux';
import Articles from './articles';

import { fetchArticles, updateFilter, fetchFeaturedArticle } from './articles.actions';
import { toggleSearch } from '../search/search.actions';


/*
This is a redux specific function.
What is does is: It gets the state specified in here from the global redux state.
For example, here we are retrieving the list of items from the redux store.
Whenever this list changes, any component that is using this list of item will re-render.
 */
const mapStateToProps = state => ({
  articles: state.rootReducer.articlesReducer.allArticles,
  featuredArticle: state.rootReducer.articlesReducer.featuredArticle,
  currentPage: state.rootReducer.articlesReducer.currentPage,
  totalPages: state.rootReducer.articlesReducer.totalPages,
  filter: state.rootReducer.articlesReducer.filter,
  categories: state.rootReducer.searchReducer.recommendedCategories,
});

/*
This is a redux specific function.
http://redux.js.org/docs/api/bindActionCreators.html
 */
const mapDispatchToProps = dispatch => ({
  getArticles: ({pageNum}) => dispatch(fetchArticles({ pageNum })),
  setFilter: (filter) => dispatch(updateFilter(filter)),
  getFeaturedArticle: () => dispatch(fetchFeaturedArticle()),
  onToggleSearch: value => dispatch(toggleSearch(value)),
});

/*
Here we are creating a Higher order component
https://facebook.github.io/react/docs/higher-order-components.html
 */
export default connect(mapStateToProps, mapDispatchToProps)(Articles);
