import { get } from 'axios';
import { createActions } from 'redux-actions';

export const {
  fetchArticlesRequest,
  fetchArticlesSuccess,
  fetchArticlesFailure,
} = createActions({
  FETCH_ARTICLES_REQUEST: () => ({ error: false, fetching: true }),
  FETCH_ARTICLES_SUCCESS: response => ({
    error: false,
    fetching: false,
    response,
  }),
  FETCH_ARTICLES_FAILURE: error => ({ error, fetching: false }),
});

export const fetchArticles = ({ pageNum = 1, filter }) => (dispatch, getState) => {
  const state = getState();
  const requestFilter = filter || state.rootReducer.articlesReducer.filter;
  dispatch(fetchArticlesRequest());

  return get(`/api/posts?page=${pageNum}&filter=${requestFilter}`)
    .then(response => dispatch(fetchArticlesSuccess(response)))
    .catch(error => dispatch(fetchArticlesFailure(error)));
};

export const {
  fetchArticleRequest,
  fetchArticleSuccess,
  fetchArticleFailure,
} = createActions({
  FETCH_ARTICLE_REQUEST: () => ({ error: false, fetching: true }),
  FETCH_ARTICLE_SUCCESS: response => ({
    error: false,
    fetching: false,
    response,
  }),
  FETCH_ARTICLE_FAILURE: error => ({ error, fetching: false }),
});

export const fetchArticle = id => (dispatch) => {
  dispatch(fetchArticleRequest());

  return get(`/api/posts/${id}`)
    .then(response => dispatch(fetchArticleSuccess(response)))
    .catch(error => dispatch(fetchArticleFailure(error)));
};

export const {
  fetchFeaturedArticleRequest,
  fetchFeaturedArticleSuccess,
  fetchFeaturedArticleFailure,
} = createActions({
  FETCH_FEATURED_ARTICLE_REQUEST: () => ({ error: false, fetching: true }),
  FETCH_FEATURED_ARTICLE_SUCCESS: response => ({
    error: false,
    fetching: false,
    response,
  }),
  FETCH_FEATURED_ARTICLE_FAILURE: error => ({ error, fetching: false }),
});

export const fetchFeaturedArticle = () => (dispatch) => {
  dispatch(fetchFeaturedArticleRequest());

  return get(`/api/posts/featured`)
    .then(response => dispatch(fetchFeaturedArticleSuccess(response)))
    .catch(error => dispatch(fetchFeaturedArticleFailure(error)));
};

export const {
  setFilter,
} = createActions({
  SET_FILTER: filter => ({ filter }),
});

export const updateFilter = (filter) => (dispatch) => {
  dispatch(fetchArticles({ pageNum: 1, filter }));
};

export const {
  fetchArticleImagesRequest,
  fetchArticleImagesSuccess,
  fetchArticleImagesFailure,
} = createActions({
  FETCH_ARTICLE_IMAGES_REQUEST: () => ({ error: false, fetching: true }),
  FETCH_ARTICLE_IMAGES_SUCCESS: response => ({
    error: false,
    fetching: false,
    response,
  }),
  FETCH_ARTICLE_IMAGES_FAILURE: error => ({ error, fetching: false }),
});

export const fetchArticleImages = () => (dispatch) => {
  dispatch(fetchArticleImagesRequest());

  return get('/api/posts/images')
    .then(response => dispatch(fetchArticleImagesSuccess(response)))
    .catch(error => dispatch(fetchArticleImagesFailure(error)));
};
