import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import isUndefined from 'lodash/isUndefined';
import min from 'lodash/min';
import max from 'lodash/max';
import classNames from 'classnames';

import ReactMapGL, { NavigationControl, Marker, FlyToInterpolator } from 'react-map-gl';
import WebMercatorViewport from 'viewport-mercator-project';
import MarkerComponent from './marker';
import {
  MAP_TYPE,
  INSTAGRAM_TYPE,
  ARTICLE_HERO_TYPE,
  ARTICLE_LIST_TYPE,
} from '../maps/maps.constants';


const MAPBOX_ACCESS_TOKEN = process.env.MAPBOX_ACCESS_TOKEN; // eslint-disable-line

if (!MAPBOX_ACCESS_TOKEN) {
  throw new Error('Please specify a valid mapbox token');
}

class MapContainer extends Component {
  constructor(props) {
    super(props);

    this.onViewportChange = this.onViewportChange.bind(this);
    this.calculateBounds = this.calculateBounds.bind(this);

    this.isMobile = (window.innerWidth || screen.width) < 768;

    this.state = {
      viewport: {
        width: props.width || window.innerWidth || screen.width,
        height: props.height || 500,
        latitude: 19.763120070194905,
        longitude: 20.951643069107373,
        zoom: 1.6480754279213963,
      },
      settings: {
        dragPan: true,
        dragRotate: true,
        scrollZoom: true,
        touchZoom: true,
        touchRotate: true,
        doubleClickZoom: true,
        minZoom: 0,
        maxZoom: 20,
        minPitch: 0,
        maxPitch: 85,
        ...props.settings,
      },
    };
  }

  componentDidMount() {
    window.addEventListener('resize', () => this.calculateBounds());
    this.calculateBounds();
  }

  componentWillReceiveProps({
    mapType,
    markers,
    focussedMarker,
    animate = false,
    zoom,
    resetView
  }) {
    this.calculateBounds(markers, focussedMarker, animate, zoom, resetView);
    if (mapType !== ARTICLE_HERO_TYPE && mapType !== ARTICLE_LIST_TYPE) {
      const fullScreenCard =
        !isEmpty(focussedMarker) &&
        this.isMobile &&
        (mapType === MAP_TYPE || mapType === INSTAGRAM_TYPE);
      this.state.settings.scrollZoom = !fullScreenCard;
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.calculateBounds);
  }

  onViewportChange(viewport) {
    this.setState({
      viewport: { ...this.state.viewport, ...viewport },
    });
  }

  calculateBounds(markers, focussedMarker, animate, zoom, resetView) {
    const width = this.props.width || window.innerWidth || screen.width;
    const height = this.props.height || 500;
    const mapFocussedMarker = focussedMarker || this.props.focussedMarker;
    const mapMarkers = markers || this.props.markers;
    const mapAnimate = animate || this.props.animate;
    const mapZoom = zoom || this.props.zoom || this.state.viewport.zoom;
    const mapResetView = isUndefined(resetView) ? true : resetView;

    if (isEmpty(mapMarkers)) return;

    if (!mapResetView) return;

    if (mapMarkers.length === 1 || !isEmpty(focussedMarker)) {
      const singleMarker = !isEmpty(focussedMarker) ? focussedMarker : mapMarkers[0];

      return this.onViewportChange({
        width,
        height,
        longitude: singleMarker.location.geo[0],
        latitude: singleMarker.location.geo[1],
        zoom: mapZoom,
        transitionDuration: 1000,
        transitionInterpolator: new FlyToInterpolator(),
      });
    } else {
      const viewport = new WebMercatorViewport({
        width,
        height,
      });

      const longitudes = mapMarkers.map(place => place.location.geo && place.location.geo[0]);
      const latitudes = mapMarkers.map(place => place.location.geo && place.location.geo[1]);

      const minLong = min(longitudes);
      const maxLong = max(longitudes);

      const minLat = min(latitudes);
      const maxLat = max(latitudes);

      const bound = viewport.fitBounds(
        [[minLong, minLat], [maxLong, maxLat]],
        { padding: 80, offset: [0, 0] },
      );

      return this.onViewportChange({
        ...bound,
        transitionInterpolator: new FlyToInterpolator(),
        transitionDuration: 1000,
      });
    }
  }

  renderMapMarkers() {
    const { markers, focussedMarker, context, mapType } = this.props;

    if (isEmpty(markers)) return null;

    return markers.map((marker, i) => {
      const { location: { geo } } = marker;
      marker.context = context;
      const focussed =
        !isEmpty(focussedMarker) &&
        (
          (focussedMarker._id && focussedMarker._id === marker._id) ||
          (focussedMarker.id && focussedMarker.id === marker.id)
        );
      const fullScreenCard =
        focussed &&
        this.isMobile &&
        (mapType === MAP_TYPE || mapType === INSTAGRAM_TYPE);

      return (
        <Marker
          className={classNames({
            fullScreenCard,
            focussed,
          })}
          key={i}
          longitude={geo[0]}
          latitude={geo[1]}
        >
          <MarkerComponent {...marker} />
        </Marker>
      );
    });
  }

  render() {
    const { viewport, settings } = this.state;

    return (
      <ReactMapGL
        {...viewport}
        {...settings}
        mapboxApiAccessToken={MAPBOX_ACCESS_TOKEN}
        mapStyle="mapbox://styles/marlonkenny/cjbiib7lu0ecy2sllrgfjydir"
        onViewportChange={this.onViewportChange}
      >
        <NavigationControl
          onViewportChange={this.onViewportChange}
        />
        {this.renderMapMarkers()}
      </ReactMapGL>
    );
  }
}

export default MapContainer;
