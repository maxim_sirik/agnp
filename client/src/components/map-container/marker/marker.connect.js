import { connect } from 'react-redux';
import Marker from './marker';
import { goToCity, goToPlace, setFocussedMarker } from '../../maps/maps.actions';

/*
This is a redux specific function.
What is does is: It gets the state specified in here from the global redux state.
For example, here we are retrieving the list of items from the redux store.
Whenever this list changes, any component that is using this list of item will re-render.
 */
const mapStateToProps = state => ({
  mapType: state.rootReducer.mapsReducer.mapType,
  focussedMarker: state.rootReducer.mapsReducer.focussedMarker,
});

/*
This is a redux specific function.
http://redux.js.org/docs/api/bindActionCreators.html
 */
const mapDispatchToProps = dispatch => ({
  onGoToCity: slug => dispatch(goToCity(slug)),
  onGoToPlace: placeId => dispatch(goToPlace(placeId)),
  onFocusCard: ({ marker, zoom, resetView }) =>
    dispatch(setFocussedMarker(marker, zoom, resetView)),
});

/*
Here we are creating a Higher order component
https://facebook.github.io/react/docs/higher-order-components.html
 */
export default connect(mapStateToProps, mapDispatchToProps)(Marker);
