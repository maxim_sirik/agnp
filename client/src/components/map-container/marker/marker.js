import React, { Component } from 'react';
import classNames from 'classnames';
import Truncate from 'react-truncate';

import { Image, Transformation } from 'cloudinary-react';
import { instagramIdToUrlSegment } from 'instagram-id-to-url-segment';
import MapMarkerIcon from 'react-icons/lib/fa/map-marker';
import CloseIcon from 'react-icons/lib/md/close';

import InstagramIcon from '../../../icons/instagram';

import { MAP_TYPE, INSTAGRAM_TYPE, CITY_TYPE } from '../../maps/maps.constants';

const calculateAspectRatio = () => {
  const width = window.innerWidth || screen.width;
  const height = window.innerHeight || screen.height;

  return width < 768 ? width / height : 0.75;
};

class Marker extends Component {
  constructor(props) {
    super(props);

    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  handleClickOutside(event) {
    if (this.markerCard && !this.markerCard.contains(event.target)) {
      this.closeCard({
        marker: {},
        resetView: this.props.mapType !== INSTAGRAM_TYPE
      });
    }
  }

  openCard(options) {
    document.addEventListener('mousedown', this.handleClickOutside);
    return this.props.onFocusCard(options)
  }

  closeCard(options) {
    document.removeEventListener('mousedown', this.handleClickOutside);
    return this.props.onFocusCard(options)
  }

  renderArticlePlaceMarker() {
    const { title, number } = this.props;

    return (
      <div className="article-map-marker">
        <MapMarkerIcon
          onClick={() => onFocusCard({ marker: this.props })}
        />
        <h3 className="number">{number}</h3>
        <h3 className="title">{title}</h3>
      </div>
    );
  }

  renderInstagramCard() {
    const { focussedMarker, onFocusCard, title, slug, image, id, caption } = this.props;
    const cardOpen = focussedMarker.id === id;

    return (
      <div ref={(ref) => this.markerCard = ref}>
        <MapMarkerIcon
          className={classNames('map-marker instagram-marker', { focussed: cardOpen })}
          onClick={() => this.openCard({ marker: this.props, zoom: 12 })}
        />
        <div className={classNames('map-card instagram-map-card', { focussed: cardOpen })}>
          <span
            className="map-card-close"
            onClick={() => this.closeCard({marker: {}, resetView: false })}
          >
            <CloseIcon />
          </span>
          <div className="map-card-instagram-image">
            <img src={image.url} />
          </div>
          <div className="map-card-caption">
            <div className="map-card-caption-profile">
              <img className="profile-image" src={caption.from.profile_picture} />
              <div className="profile-info">
                <span className="profile-name">
                  aguynamedpatrick
                  <img className="verified-badge" src="https://res.cloudinary.com/agnp/image/upload/v1521000383/verified-logo_zwrrif.png" />
                </span>
                <span className="profile-post-location">New York, New York</span>
              </div>
            </div>
            <hr className="map-card-divider" />
            <p className="map-card-caption-text">
              <Truncate lines={3} ellipsis="...">
                {caption.text}
              </Truncate>
            </p>
            <a
              className="instagram-link"
              href={`https://instagram.com/p/${instagramIdToUrlSegment(id.split('_')[0])}`}
              target="_blank"
            >
              <InstagramIcon /> SEE ON INSTAGRAM
            </a>
          </div>
        </div>
      </div>
    );
  }

  renderMapCard() {
    const { onFocusCard, focussedMarker, _id, title, slug, image, onGoToCity } = this.props;
    const cardOpen = focussedMarker._id === _id;

    return (
      <div ref={(ref) => this.markerCard = ref}>
        <MapMarkerIcon
          className={classNames('map-marker city-marker', { focussed: cardOpen })}
          onClick={() => this.openCard({ marker: this.props })}
        />
        <div className={classNames('map-card', { focussed: cardOpen })}>
          <span
            className="map-card-close"
            onClick={() => this.closeCard({ marker: {} })}
          >
            <CloseIcon />
          </span>
          {image &&
            <img
              className="map-card-map-image"
              src={`https://res.cloudinary.com/agnp/image/upload/ar_${calculateAspectRatio()},c_crop,dpr_auto,f_auto,q_auto/c_scale/${image.public_id}`}
            />
          }
          <div className="map-card-info">
            <hr />
            <h3 className="map-card-map-text">{title}</h3>
            <a
              className="map-card-link"
              onClick={() => {
                this.closeCard({ marker: {} });
                return onGoToCity(slug);
              }}
            >
              SEE PLACES
            </a>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { onGoToPlace, context, mapType, focussedMarker, _id, id } = this.props;

    if (context === 'article') {
      return this.renderArticlePlaceMarker();
    } else if (mapType === MAP_TYPE) {
      return this.renderMapCard(focussedMarker._id === _id);
    } else if (mapType === INSTAGRAM_TYPE) {
      return this.renderInstagramCard(focussedMarker.id === id);
    } else if (mapType === CITY_TYPE) {
      return (
      <MapMarkerIcon
        className="map-marker city-place-marker"
        onClick={() => onGoToPlace(this.props._id)}
      />
    );
    } else {
      return (
        <MapMarkerIcon className="map-marker"/>
      )
    }
  }
}

export default Marker;
