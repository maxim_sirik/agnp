import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Image, Transformation } from 'cloudinary-react';
import classNames from 'classnames';
import CloseIcon from 'react-icons/lib/md/close';
import Search from './';

class Search extends Component {
  constructor(props) {
    super(props);

    this.onCloseSearch = this.onCloseSearch.bind(this);
  }

  onCloseSearch() {
    this.props.onToggleSearch(false);
    this.setState({ searching: false });
    this.searchInput.value = '';
  }

  render() {
    const { displaySearch, results, onToggleSearch, recommendedCategories } = this.props;

    return (
      <div className={classNames('search search-header', { show: displaySearch, searching: this.state.searching })}>
        <CloseIcon className="search-close" onClick={this.onCloseSearch} />
        <Search />
      </div>
    );
  }
};

export default Search;
