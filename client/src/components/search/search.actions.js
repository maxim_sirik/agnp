import { get } from 'axios';
import { createActions } from 'redux-actions';

export const {
  fetchSearchRequest,
  fetchSearchSuccess,
  fetchSearchFailure,
  toggleSearchModal,
} = createActions({
  FETCH_SEARCH_REQUEST: () => ({ error: false, fetching: true }),
  FETCH_SEARCH_SUCCESS: response => ({
    error: false,
    fetching: false,
    response,
  }),
  FETCH_SEARCH_FAILURE: error => ({ error: error, fetching: false }),
  TOGGLE_SEARCH_MODAL: value => ({ value }),
});

export const submitSearch = query => (dispatch) => {
  dispatch(fetchSearchRequest());

  return get(`/api/search?query=${query}`)
    .then(response => dispatch(fetchSearchSuccess(response.data)))
    .catch(error => dispatch(fetchSearchFailure(error)));
};

export const toggleSearch = value => (dispatch) => {
  document.body.style.overflow = value ? "hidden" : "scroll";
  return dispatch(toggleSearchModal(value));
}


export const {
  recommendedCategoriesRequest,
  recommendedCategoriesSuccess,
  recommendedCategoriesFailure,
} = createActions({
  RECOMMENDED_CATEGORIES_REQUEST: () => ({ error: false, fetching: true }),
  RECOMMENDED_CATEGORIES_SUCCESS: response => ({
    error: false,
    fetching: false,
    response,
  }),
  RECOMMENDED_CATEGORIES_FAILURE: error => ({ error: error, fetching: false }),
});

export const getRecommendedCategories = query => (dispatch) => {
  dispatch(recommendedCategoriesRequest());

  return get(`/api/categories`)
    .then(response => dispatch(recommendedCategoriesSuccess(response.data)))
    .catch(error => dispatch(recommendedCategoriesFailure(error)));
};