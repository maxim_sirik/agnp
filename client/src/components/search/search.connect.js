import { connect } from 'react-redux';
import Search from './search';

import { submitSearch, toggleSearch, getRecommendedCategories } from './search.actions';

/*
This is a redux specific function.
What is does is: It gets the state specified in here from the global redux state.
For example, here we are retrieving the list of items from the redux store.
Whenever this list changes, any component that is using this list of item will re-render.
 */
const mapStateToProps = state => ({
  results: state.rootReducer.searchReducer.results,
  displaySearch: state.rootReducer.searchReducer.displaySearch,
  recommendedCategories: state.rootReducer.searchReducer.recommendedCategories,
});

/*
This is a redux specific function.
http://redux.js.org/docs/api/bindActionCreators.html
 */
const mapDispatchToProps = dispatch => ({
  onSubmitSearch: query => dispatch(submitSearch(query)),
  onToggleSearch: value => dispatch(toggleSearch(value)),
  onGetRecommendedCategories: () => dispatch(getRecommendedCategories())
});

/*
Here we are creating a Higher order component
https://facebook.github.io/react/docs/higher-order-components.html
 */
export default connect(mapStateToProps, mapDispatchToProps)(Search);
