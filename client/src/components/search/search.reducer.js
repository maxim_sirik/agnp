import { handleActions, combineActions } from 'redux-actions';
import {
  fetchSearchRequest,
  fetchSearchSuccess,
  fetchSearchFailure,
  toggleSearchModal,
  recommendedCategoriesRequest,
  recommendedCategoriesSuccess,
  recommendedCategoriesFailure,
} from './search.actions';

const initialState = {
  results: [],
  displaySearch: false,
  fetching: null,
  error: null,
};

export default handleActions(
  {
    [toggleSearchModal]: (state, { payload: { value } }) => ({
      ...state,
      results: [],
      displaySearch: value,
    }),
    [fetchSearchSuccess]: (state, { payload: { error, fetching, response } }) => ({
      ...state,
      error,
      fetching,
      results: response,
    }),
    [recommendedCategoriesSuccess]: (state, { payload: { error, fetching, response } }) => ({
      ...state,
      error,
      fetching,
      recommendedCategories: response,
    }),
    [combineActions(
      fetchSearchRequest,
      fetchSearchFailure,
      recommendedCategoriesRequest,
      recommendedCategoriesFailure)](
      state,
      { payload: { error, fetching } }
    ) {
      return { ...state, error, fetching };
    },
  },
  initialState,
);
