import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import { Link } from 'react-router-dom';
import { Image, Transformation } from 'cloudinary-react';
import classNames from 'classnames';
import CloseIcon from 'react-icons/lib/md/close';

const CATEGORIES = [
  'New York City',
  'Los Angeles',
  'Brunch',
  'Hidden Spots',
  'Cocktails'
]
class Search extends Component {
  constructor(props) {
    super(props);

    this.onInputChange = this.onInputChange.bind(this);
    this.onCloseSearch = this.onCloseSearch.bind(this);

    this.state = {
      searching: false,
    }
  }

  componentDidMount() {
    this.props.onGetRecommendedCategories();
  }

  componentDidUpdate() {
    this.searchInput.focus();
  }

  onCloseSearch() {
    this.props.onToggleSearch(false);
    this.setState({ searching: false });
    this.searchInput.value = '';
  }

  onInputChange() {
    const inputValue = this.searchInput.value;

    this.setState({ searching: inputValue === '' ? false : true })
    this.props.onSubmitSearch(inputValue);
  }

  searchByCategory(category) {
    this.searchInput.value = category;
    this.onInputChange();
  }

  renderResult(result) {
    if (result.city) {
      return (
        <Link
          onClick={this.onCloseSearch}
          className="search-result"
          to={`/maps/${result.city.slug}`}
        >
          {result.image ?
            <img src={`https://res.cloudinary.com/agnp/image/upload/ar_1,c_crop,dpr_auto,f_auto,q_auto/c_scale/${result.image.public_id}`} />
            :
            <div className="image-placeholder" />
          }
            <div className="article-category">
              <hr />
              <span>CITY GUIDE</span>
            </div>
          <h3>{result.title}</h3>
        </Link>
      );
    } else if (result.location) {
      return (
        <Link
          onClick={this.onCloseSearch}
          className="search-result"
          to={`/maps/${result.slug}`}
        >
          {result.image ?
            <img src={`https://res.cloudinary.com/agnp/image/upload/ar_1,c_crop,dpr_auto,f_auto,q_auto/c_scale/${result.image.public_id}`} />
            :
            <div className="image-placeholder" />
          }
            <div className="article-category">
              <hr />
              <span>CITY GUIDE</span>
            </div>
          <h3>{result.title}</h3>
        </Link>
      );
    } else {
      return (
        <Link
          onClick={this.onCloseSearch}
          className="search-result"
          to={`/articles/${result.slug}`}
        >
          {result.heroImage.image ?
            <img src={`https://res.cloudinary.com/agnp/image/upload/ar_1,c_crop,dpr_auto,f_auto,q_auto/c_scale/${result.heroImage.image.public_id}`} />
            :
            <div className="image-placeholder" />
          }
          {result.categories[0] &&
            <div className="article-category">
              <hr />
              <span>{result.categories[0].name}</span>
            </div>
          }
          <h3>{result.title}</h3>
        </Link>
      );
    }
  }

  render() {
    const { displaySearch, results, onToggleSearch, recommendedCategories } = this.props;

    return (
      <div className={classNames('search', { show: displaySearch, searching: this.state.searching })}>
        <CloseIcon className="search-close" onClick={this.onCloseSearch} />
        <h2>What would you like to discover?</h2>
        <input
          className="search-input"
          type="text"
          ref={(input) => { this.searchInput = input; }}
          onChange={this.onInputChange}
        />
        {!isEmpty(results) && (!isEmpty(results.tagMatches) || !isEmpty(results.otherMatches)) ?
          <div className="results-container">
            {!isEmpty(results.tagMatches) &&
              <div>
                <h3 className="tag-header">Relevant results for "{this.searchInput.value}":</h3>
                <div className="results tag-matches">
                  {results.tagMatches.map((result) => this.renderResult(result))}
                </div>
              </div>
            }
            {!isEmpty(results.otherMatches) &&
              <div>
                {!isEmpty(results.tagMatches) &&
                  <h3 className="other-header">
                    "{this.searchInput.value}" also appears in the following articles:
                  </h3>
                }
                <div className="results other-matches">
                  {results.otherMatches.map((result) => this.renderResult(result))}
                </div>
              </div>
            }
          </div>
          :
          <div className="tags">
            <span className="tags-title">RECOMMENDED TAGS</span>
            <span className="tags-list">
              {CATEGORIES.map((category) =>
                <span
                  className="tag"
                  onClick={() => this.searchByCategory(category)
                }>
                  {category}
                </span>
              )}
            </span>
          </div>
        }
      </div>
    );
  }
};

export default Search;
