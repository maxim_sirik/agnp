import { post } from 'axios';
import { createActions } from 'redux-actions';

export const {
  submitEmailRequest,
  submitEmailSuccess,
  submitEmailFailure,
} = createActions({
  SUBMIT_EMAIL_REQUEST: () => ({ error: false, fetching: true }),
  SUBMIT_EMAIL_SUCCESS: response => ({
    error: false,
    fetching: false,
    response,
  }),
  SUBMIT_EMAIL_FAILURE: error => ({ error: error, fetching: false }),
});

export const submitEmail = email => (dispatch) => {
  dispatch(submitEmailRequest());

  return post('/api/email', { email })
    .then(response => dispatch(submitEmailSuccess(response.data)))
    .catch(error => dispatch(submitEmailFailure(error)));
};