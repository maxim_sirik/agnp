import { handleActions, combineActions } from 'redux-actions';
import get from 'lodash/get';

import {
  submitEmailRequest,
  submitEmailSuccess,
  submitEmailFailure,
} from './footer.actions';

const initialState = {
  emailSubmitted: false,
};

export default handleActions(
  {
    [submitEmailSuccess]: (state, { payload: { response } }) => ({
      ...state,
      emailSubmitted: response.message === 'success',
    }),
    [combineActions(
      submitEmailRequest,
      submitEmailFailure,
    )](
      state,
      { payload: { error, fetching } }
    ) {
      return { ...state, error, fetching };
    },
  },
  initialState,
);
