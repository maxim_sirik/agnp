import { connect } from 'react-redux';
import { withRouter } from 'react-router'

import About from './about';
import {
    fetchContent,
    fetchAboutArticles
} from './about.actions';

/*
This is a redux specific function.
What is does is: It gets the state specified in here from the global redux state.
For example, here we are retrieving the list of items from the redux store.
Whenever this list changes, any component that is using this list of item will re-render.
 */
const mapStateToProps = state => ({
    content: state.rootReducer.aboutReducer.content,
    articles: state.rootReducer.aboutReducer.articles
});

/*
This is a redux specific function.
http://redux.js.org/docs/api/bindActionCreators.html
 */
const mapDispatchToProps = dispatch => ({
    getContent: () => dispatch(fetchContent()),
    getAboutArticles: () => dispatch(fetchAboutArticles()),
});

/*
Here we are creating a Higher order component
https://facebook.github.io/react/docs/higher-order-components.html
 */
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(About));