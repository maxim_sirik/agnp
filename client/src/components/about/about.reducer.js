import { handleActions, combineActions } from 'redux-actions';
import {
    fetchContentRequest, fetchContentSuccess, fetchContentFailure,
    fetchAboutArticlesRequest, fetchAboutArticlesSuccess, fetchAboutArticlesFailure,
} from "../about/about.actions";

const initialState = {
    content: {},
    articles: [],
    fetching: null,
    error: null,
};

export default handleActions(
    {
        [fetchContentSuccess]: (state, { payload: { error, fetching, response } }) => ({
            ...state,
            error,
            fetching,
            content: response.data,
        }),
        [fetchAboutArticlesSuccess]: (state, { payload: { error, fetching, response } }) => ({
            ...state,
            error,
            fetching,
            articles: response.data,
        }),
        [combineActions(
            fetchContentRequest,
            fetchContentFailure,
            fetchAboutArticlesRequest,
            fetchAboutArticlesFailure,
        )](
            state,
            { payload: { error, fetching } }
        ) {
            return { ...state, error, fetching };
        },
    },
    initialState,
);