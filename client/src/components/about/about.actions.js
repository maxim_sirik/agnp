import { get } from 'axios';
import compact from 'lodash/compact';
import findIndex from 'lodash/findIndex';
import isEmpty from 'lodash/isEmpty';
import { createActions } from 'redux-actions';
import { push } from 'react-router-redux';

export const {
    fetchContentRequest,
    fetchContentSuccess,
    fetchContentFailure,
    fetchAboutArticlesRequest,
    fetchAboutArticlesSuccess,
    fetchAboutArticlesFailure
} = createActions({
    FETCH_CONTENT_REQUEST: () => ({ error: false, fetching: true }),
    FETCH_CONTENT_SUCCESS: response => ({
        error: false,
        fetching: false,
        response,
    }),
    FETCH_CONTENT_FAILURE: error => ({ error, fetching: false }),
    FETCH_ABOUT_ARTICLES_REQUEST: () => ({ error: false, fetching: true }),
    FETCH_ABOUT_ARTICLES_SUCCESS: response => ({
        error: false,
        fetching: false,
        response,
    }),
    FETCH_ABOUT_ARTICLES_FAILURE: error => ({ error, fetching: false }),
});

export const fetchContent = () => (dispatch) => {
    dispatch(fetchContentRequest());

    return get('/api/about')
        .then(response => dispatch(fetchContentSuccess(response)))
        .catch(error => dispatch(fetchContentFailure(error)));
};

export const fetchAboutArticles = () => (dispatch) => {
    dispatch(fetchAboutArticlesRequest());

    return get('/api/about/articles')
        .then(response => dispatch(fetchAboutArticlesSuccess(response)))
        .catch(error => dispatch(fetchAboutArticlesFailure(error)));
};
