import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import MediaQuery from 'react-responsive';
import { Video, Transformation } from 'cloudinary-react';
import each from 'lodash/each';
import isEmpty from 'lodash/isEmpty';

import Instagram from '../instagram';
import Footer from '../footer';
import parse from 'html-react-parser';

class About extends Component {
  constructor(props) {
    super(props);

    this.onScrollAnimations = this.onScrollAnimations.bind(this);

    this.pressRefs = [];
  }

  componentWillMount() {
    const { getContent, getAboutArticles } = this.props;

    getContent();
    getAboutArticles();
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScrollAnimations);
  }

  onScrollAnimations() {
    return !isEmpty(this.pressRefs) && each(this.pressRefs, (pressItem, index) => {
      if (!pressItem) return;
      const distanceFromBottom = window.innerHeight - pressItem.getBoundingClientRect().y;

      if (distanceFromBottom > -10) {
        pressItem.style.top = 0;
      } else if (distanceFromBottom < 0) {
        pressItem.style.top = '200px';
      }
    })
  }

  render() {
    const pageWidth = window.innerWidth || screen.width;

    const { content, articles } = this.props;

    return (
      <div>
        <div className="about">
          <div className="hero">
            {
              (content.video) ? (
                <React.Fragment>
                  <MediaQuery maxWidth={1100}>
                    <Video publicId={content.video.publicId1} width={pageWidth} height="500" crop="fill"  loop autoPlay playsInline />
                  </MediaQuery>
                  <MediaQuery minWidth={1101}>
                    <Video publicId={content.video.publicId2} width={pageWidth} loop autoPlay playsInline />
                  </MediaQuery>
                </React.Fragment>
              ) : ''
            }
          </div>
          <div className="about-body">
            {
              (content.quote) ? (
                  <div className="about-quote">
                      <h2 dangerouslySetInnerHTML={{
                        __html: content.quote
                      }}></h2>
                  </div>
              ) : ''
            }
            <div className="about-body-text">
              {
                 (content.aboutContent) ? (parse(content.aboutContent)) : ''
              }
              {
                 (content.signatureImage) ? (<img src={content.signatureImage.secure_url} />) : ''
              }
            </div>
            <div className="about-contact">
              {
                 (content.contactTitle) ? ( <h2>{content.contactTitle}</h2>) : ''
              }
              <hr />
                {
                    (content.ContactLink && content.ContactLinkLabel) ? (
                        <a href={content.ContactLink} className="about-contact-button">{content.ContactLinkLabel}</a>
                    ) : ''
                }
            </div>
            {
               (content.articlesSectionTitle) ? (<h2 className="about-press-title">{content.articlesSectionTitle}</h2>) : ''
            }
            {
              (articles.length > 0) ? (
                  <div className="about-press">
                      {
                        articles.map((article, idx) => (
                          <div className="press-item" key={'about_press_' + idx} ref={ref => this.pressRefs[idx] = ref}>
                            <a href={article.heroImage.link} target="_blank">
                              <div className="press-image">
                                <img src={article.heroImage.image.secure_url} />
                              </div>
                            </a>
                            <div className="press-logo">
                              {
                                (article.logo.image) ? (
                                  <img height={ (article.logo.height40) ? 40 : '' } src={article.logo.image.secure_url} />
                                ) : ''
                              }
                              {
                                (article.logo.svgCode && !article.logo.image) ? (parse(article.logo.svgCode)) : ''
                              }
                            </div>
                            <p className="press-brief">{article.brief}</p>
                            <a className="see-more" href={article.url} target="_blank">READ MORE</a>
                          </div>
                        ))
                      }
                </div>
              ) : ''
            }
          </div>
          <Instagram />
        </div>
        <Footer />
      </div>
    );
  }
};

export default About;
