import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter, routerMiddleware, routerReducer } from 'react-router-redux'
import { AppContainer } from 'react-hot-loader';
import { ParallaxProvider } from 'react-scroll-parallax';
import rootReducer from './reducers/index';
import ScrollToTop from './scrollToTop';
import App from './app';

// Create a history
const history = createHistory();

/*
Here we are getting the initial state injected by the server. See routes/index.js for more details
 */
const initialState = window.__INITIAL_STATE__; // eslint-disable-line

const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];

const devToolsExtension = window.devToolsExtension;

if (typeof devToolsExtension === 'function') {
  enhancers.push(devToolsExtension());
}

/* === DEV_MIDDLEWARES === */
let devMiddlewares = [];

middleware.push(...devMiddlewares);

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

const store = createStore(
  combineReducers({
    rootReducer,
    router: routerReducer,
  }),
  composedEnhancers,
);

/*
While creating a store, we will inject the initial state we received from the server to our app.
 */
const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <ConnectedRouter history={history} onUpdate={() => window.scrollTo(0, 0)}>
          <ScrollToTop>
            <ParallaxProvider>
              <Component />
            </ParallaxProvider>
          </ScrollToTop>
        </ConnectedRouter>
      </Provider>
    </AppContainer>,
    document.getElementById('reactbody'),
  );
};

render(App);

if (module.hot) {
  module.hot.accept('./app', () => {
    // eslint-disable-next-line
    const nextApp = require('./app').default;
    render(nextApp);
  });
}

// module.hot.accept('./reducers', () => {
//   // eslint-disable-next-line
//   const nextRootReducer = require('./reducers/index');
//   store.replaceReducer(nextRootReducer);
// });
