import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { CloudinaryContext } from 'cloudinary-react';
import ReactGA from 'react-ga';
import Cookies from 'universal-cookie';

import NewsletterModal from './components/newsletter-modal';
import Header from './components/header';
import Footer from './components/footer';
import Home from './components/home';
import About from './components/about';
import Articles from './components/articles';
import Article from './components/articles/article';
import Maps from './components/maps';
import Privacy from './components/privacy';

const NEWSLETTER_COOKIE_ID = 'newsletterFormDisplayCount';

const DEFAULT_CONFIG = {
    trackingId: 'UA-119908375-1',
    debug: false,
    gaOptions: {
        cookieDomain: 'none',
        standardImplementation: true,
    }
};

class App extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            reactGaInitialised: false,
            configs: DEFAULT_CONFIG,
            newsletterModalOpen: true
        };
    }

    componentWillMount() {
        const cookies = new Cookies();
        const displayCount = cookies.get(NEWSLETTER_COOKIE_ID) || 0;
        const expires = new Date(new Date().setFullYear(new Date().getFullYear() + 1));

        this.setState({ newsletterModalOpen: displayCount < 2 });
        cookies.set(
            NEWSLETTER_COOKIE_ID,
            parseInt(displayCount) + 1,
            { expires },
        );
    }

    componentDidMount() {
        if (!this.state.reactGaInitialised) {
            ReactGA.initialize(this.state.configs.trackingId, this.state.configs.gaOptions);
            ReactGA.pageview(window.location.pathname + window.location.search);
            this.setState({ reactGaInitialised: true });
        }
    }

    componentWillReceiveProps(nextProps) {
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    render() {
        return (
            <div>
                <CloudinaryContext cloudName="agnp">
                    {this.state.newsletterModalOpen
                    && <NewsletterModal onClickClose={() => this.setState({ newsletterModalOpen: false })} />
                    }
                    <Header />
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/about" component={About} />
                        <Route exact path="/articles" component={Articles} />
                        <Route exact path="/privacy" component={Privacy} />
                        <Route path="/articles/:slug" component={Article} />
                        <Route path="/maps" component={Maps} />
                        <Route render={() => <h1>404</h1>} />
                    </Switch>
                </CloudinaryContext>
            </div>
        );
    }
};

export default App;